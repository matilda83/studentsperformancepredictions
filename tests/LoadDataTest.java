package tests;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import gai.LoadData;
import gai.Student;
import gai.StudentCollection;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 20.08.13
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
public class LoadDataTest {

    @Test
    public void testLineAnalysisForStudentDetails() {
        StudentCollection studentCollection = new StudentCollection();
        ArrayList<String> lines = new ArrayList<String>();
        lines.add("201114097\t3\tHome student\tUG\tContinuing student\t01");
        lines.add("198113114	1	Home student	PG	New student	01");
        lines.add("198108018\t1\tOverseas student\tPG\tNew student\t01");

        ArrayList<Integer> janStarters = new ArrayList<Integer>();
        janStarters.add(198113114);

        for (String line : lines) {
            LoadData.lineAnalysisForStudentDetails(studentCollection, line, janStarters);
        }

        Assert.assertEquals("the size of array two", 2, studentCollection.size());
        Assert.assertEquals(true, studentCollection.get(198113114).isHomeStudent);
        Assert.assertEquals(false, studentCollection.get(198113114).isSeptemberStarter);
        Assert.assertEquals(true, studentCollection.get(198113114).isFullTimeStudent);
        Assert.assertEquals(false, studentCollection.get(198108018).isHomeStudent);
        Assert.assertEquals(true, studentCollection.get(198108018).isSeptemberStarter);
        Assert.assertEquals(true, studentCollection.get(198108018).isFullTimeStudent);
    }

    @Test
    public void testLineAnalysisForAttendance() {

        StudentCollection studentList = new StudentCollection();
        studentList.get(197102132);
        studentList.get(197117004);

        String line = "197102132,P03504,1,137,168,81.5";

        LoadData.lineAnalysisForAttendance(studentList, line);

        Student st = studentList.get(197102132);

        Assert.assertEquals("expected array size", 1, st.attendencePercentage.size());
        Assert.assertEquals(Float.valueOf("81.5"), st.attendencePercentage.get(st.attendencePercentage.size() - 1), 0);

    }

    @Test
    public void testLineAnalysisForGrades() {
        StudentCollection studentList = new StudentCollection();
        studentList.get(198111124);
        studentList.get(204107009);

        ArrayList<String> lines = new ArrayList<String>();
        lines.add("198111124\t186893\tCINE1025\t47");
        lines.add("216108101\t186895\tCINE1025\t47");
        lines.add("198111124\t186894\tCINE1025\t50");


        for (String line: lines)
        LoadData.lineAnalysisForGrades(studentList, line);

        Assert.assertEquals(2, studentList.get(198111124).grades.size());
        Assert.assertEquals(47, studentList.get(198111124).grades.get(0), 0);
        Assert.assertEquals(50, studentList.get(198111124).grades.get(1), 0);
        Assert.assertEquals(null, studentList.get(204107009).grades);

    }

    public void testLineAnalysisForPareCWGrades() {
        StudentCollection studentList = new StudentCollection();
        studentList.get(197102319);
        studentList.get(204107009);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);

        studentList.get(197102319).grades = arrayList;

        ArrayList<String> lines = new ArrayList<String>();
        lines.add("197102319\t70");
        lines.add("197102319\t5");
        lines.add("197126015\t82");


        for (String line: lines)
            LoadData.lineAnalysisForPaperCWGrades(studentList, line);

        Assert.assertEquals(4, studentList.get(197102319).grades.size());
        Assert.assertEquals(70, studentList.get(197102319).grades.get(2), 0);
        Assert.assertEquals(5, studentList.get(197102319).grades.get(3), 0);
        Assert.assertEquals(null, studentList.get(204107009).grades);

    }
}
