package tests;


import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.TreeMap;

import gai.Student;
import gai.Graph2EvidencePropagation;
import gai.AnalyseStudentAcademicPerformance;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 05.09.13
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */
public class Graph2EvidencePropagationTest {

    @Test
    public void testfindGradeProbability() {

        TreeMap<String,Float> evidenceMap = new TreeMap<>();
        evidenceMap.put("G0\tA0\tI0\tH0\tF0\tS0", 2f);
        evidenceMap.put("G0\tA0\tI0\tH0\tF0\tS1", 3f);
        evidenceMap.put("G0\tA0\tI0\tH0\tF1\tS0", 4f);
        evidenceMap.put("G0\tA0\tI0\tH1\tF0\tS0", 5f);
        evidenceMap.put("G0\tA0\tI1\tH0\tF0\tS0", 2f);
        evidenceMap.put("G0\tA1\tI0\tH0\tF0\tS0", 6f);
        evidenceMap.put("G0\tA1\tI1\tH0\tF0\tS0", 7f);
        evidenceMap.put("G0\tA1\tI0\tH1\tF0\tS0", 8f);
        evidenceMap.put("G0\tA1\tI0\tH0\tF1\tS0", 9f);
        evidenceMap.put("G0\tA1\tI0\tH0\tF0\tS1", 2f);
        evidenceMap.put("G0\tA1\tI1\tH1\tF0\tS0", 1f);
        evidenceMap.put("G0\tA1\tI1\tH0\tF1\tS0", 3f);
        evidenceMap.put("G0\tA1\tI1\tH0\tF0\tS1", 4f);
        evidenceMap.put("G0\tA1\tI1\tH1\tF1\tS0", 5f);
        evidenceMap.put("G0\tA1\tI1\tH1\tF0\tS1", 6f);
        evidenceMap.put("G0\tA1\tI1\tH1\tF1\tS1", 7f);

        evidenceMap.put("G1\tA0\tI0\tH0\tF0\tS0", 1f);
        evidenceMap.put("G1\tA0\tI0\tH0\tF0\tS1", 2f);
        evidenceMap.put("G1\tA0\tI0\tH0\tF1\tS0", 3f);
        evidenceMap.put("G1\tA0\tI0\tH1\tF0\tS0", 4f);
        evidenceMap.put("G1\tA0\tI1\tH0\tF0\tS0", 5f);
        evidenceMap.put("G1\tA1\tI0\tH0\tF0\tS0", 6f);
        evidenceMap.put("G1\tA1\tI1\tH0\tF0\tS0", 7f);
        evidenceMap.put("G1\tA1\tI0\tH1\tF0\tS0", 8f);
        evidenceMap.put("G1\tA1\tI0\tH0\tF1\tS0", 9f);
        evidenceMap.put("G1\tA1\tI0\tH0\tF0\tS1", 1f);
        evidenceMap.put("G1\tA1\tI1\tH1\tF0\tS0", 2f);
        evidenceMap.put("G1\tA1\tI1\tH0\tF1\tS0", 3f);
        evidenceMap.put("G1\tA1\tI1\tH0\tF0\tS1", 4f);
        evidenceMap.put("G1\tA1\tI1\tH1\tF1\tS0", 5f);
        evidenceMap.put("G1\tA1\tI1\tH1\tF0\tS1", 6f);
        evidenceMap.put("G1\tA1\tI1\tH1\tF1\tS1", 7f);

        Student testSt = new Student(1234556);
        testSt.attendanceCluster = 0;
        testSt.iaCluster = 0;
        testSt.isFullTimeStudent = true;
        testSt.isHomeStudent = false;
        testSt.isSeptemberStarter = true;



        ArrayList<Float> result = Graph2EvidencePropagation.findGradeProbability(evidenceMap, testSt);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(1.592827f, result.get(0),  0.00001f);
        Assert.assertEquals(1.401078f, result.get(1),  0.00001f);
    }


    @Test
    public void testIfClusterInProbabilityCloud() {

        ArrayList<Float> listOfProbabilities = new ArrayList<>();
        listOfProbabilities.add(1f);
        listOfProbabilities.add(2f);
        listOfProbabilities.add(3f);
        listOfProbabilities.add(10f);
        listOfProbabilities.add(13f);
        listOfProbabilities.add(11f);
        listOfProbabilities.add(12f);

        int gradeCluster = 5;



        boolean result = Graph2EvidencePropagation.iffClusterInProbabilityCloud(listOfProbabilities, gradeCluster);
        Assert.assertEquals(true, result);
    }
}
