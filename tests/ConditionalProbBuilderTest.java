package tests;

import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import gai.ConditionalProbBuilder;
import gai.Student;
import gai.StudentCollection;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 26.08.13
 * Time: 10:40
 * To change this template use File | Settings | File Templates.
 */
public class ConditionalProbBuilderTest {

    @Test
    public void testAddValuesToIntersection() {

        StudentCollection studentCollection = new StudentCollection();

        ArrayList<Student> list1 = new ArrayList<>();
        list1.add(studentCollection.get(200113184));
        list1.add(studentCollection.get(201114097));

        ArrayList<Student> list2 = new ArrayList<>();
        list2.add(studentCollection.get(200113184));
        list2.add(studentCollection.get(214113094));

        ArrayList<Student> list12 = new ArrayList<>();
        list12.add(studentCollection.get(200113184));
        list12.add(studentCollection.get(201114097));

        ArrayList<Student> list22 = new ArrayList<>();
        list22.add(studentCollection.get(200113183));
        list22.add(studentCollection.get(201114094));

        ArrayList<Student> result = ConditionalProbBuilder.addValueToIntersection(list1, list2);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(studentCollection.get(200113184), result.get(result.size() - 1));

        ArrayList<Student> result2 = ConditionalProbBuilder.addValueToIntersection(list12, list22);
        Assert.assertEquals(0, result2.size());
    }

    @Test
    public void testConditionalProbTableOneParOneChild() {
        StudentCollection studentList = new StudentCollection();
        studentList.clustersFullTime = new TreeMap<>();
        studentList.clustersFullTime.put(0, new ArrayList<Student>());
        studentList.clustersFullTime.put(1, new ArrayList<Student>());
        studentList.clustersFullTime.get(0).add(studentList.get(200113184));
        studentList.clustersFullTime.get(0).add(studentList.get(201114097));
        studentList.clustersFullTime.get(1).add(studentList.get(214113094));
        studentList.clustersFullTime.get(1).add(studentList.get(197105109));

        studentList.clustersOfIA = new TreeMap<>();
        studentList.clustersOfIA.put(0, new ArrayList<Student>());
        studentList.clustersOfIA.put(1, new ArrayList<Student>());
        studentList.clustersOfIA.put(2, new ArrayList<Student>());
        studentList.clustersOfIA.get(0).add(studentList.get(200113184));
        studentList.clustersOfIA.get(0).add(studentList.get(214113094));
        studentList.clustersOfIA.get(1).add(studentList.get(201114097));
        studentList.clustersOfIA.get(1).add(studentList.get(197105109));
        studentList.clustersOfIA.get(2).add(studentList.get(214113090));
        studentList.clustersOfIA.get(2).add(studentList.get(197105100));

        ConditionalProbBuilder.countOfStudents = studentList.size();

        String ch = "I";
        String p1 = "F";
        TreeMap<String, Float> result = ConditionalProbBuilder.conditionalProbTableOneParOneChild(ch, p1, studentList);
        Assert.assertEquals(6, result.size());
        Assert.assertEquals(0.166666f, result.get("I0\tF0"), 0.0001f);
        Assert.assertEquals(0.166666f, result.get("I0\tF1"), 0.0001f);
        Assert.assertEquals(0.166666f, result.get("I1\tF0"), 0.0001f);
        Assert.assertEquals(0.166666f, result.get("I1\tF1"), 0.0001f);
        Assert.assertEquals(0f, result.get("I2\tF0"), 0.0f);
        Assert.assertEquals(0f, result.get("I2\tF1"), 0.0f);
    }


}
