package tests;

import org.junit.Test;
import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import gai.Student;
import gai.OneDimensionClusterisation;
import gai.StudentCollection;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 22.08.13
 * Time: 22:42
 * To change this template use File | Settings | File Templates.
 */
public class OneDimensionalClusterisationTest {

    @Test
    public void testInitialPartitionAtt() throws IOException {
        StudentCollection studentCollection = new StudentCollection();
        Student st1 = studentCollection.get(3448734);
        st1.attendencePercentage = new ArrayList<Float>();
        st1.attendencePercentage.add(0, 34.56f);
        Student st2 = studentCollection.get(439579759);
        st2.attendencePercentage = new ArrayList<Float>();
        st2.attendencePercentage.add(0, 76.8f);
        Student st3 = studentCollection.get(3456566);
        st3.attendencePercentage = new ArrayList<Float>();
        st3.attendencePercentage.add(0, 45.8f);
        Student st4 = studentCollection.get(978768763);
        st4.attendencePercentage = new ArrayList<Float>();
        st4.attendencePercentage.add(0, 25.8f);
        Student st5 = studentCollection.get(978764565);
        st5.attendencePercentage = new ArrayList<Float>();
        st5.attendencePercentage.add(0, 99.8f);

        String st = "attendance";

        TreeMap<Integer, ArrayList<Student>> result = OneDimensionClusterisation.initialPartition(studentCollection, st, 3, 3, 3);

        Assert.assertEquals("size of cluster map" ,2, result.size());
        Assert.assertEquals("size of cluster 0", 3, result.get(0).size());
        Assert.assertEquals("size of cluster 1", 2, result.get(1).size());
        Assert.assertEquals(true, result.get(0).contains(studentCollection.get(978768763)));
        Assert.assertEquals(true, result.get(0).contains(studentCollection.get(3448734)));
        Assert.assertEquals(true, result.get(0).contains(studentCollection.get(3456566)));
        Assert.assertEquals(true, result.get(1).contains(studentCollection.get(439579759)));
        Assert.assertEquals(true, result.get(1).contains(studentCollection.get(978764565)));

        Assert.assertEquals(25.8f, studentCollection.get(978768763).avgSumAttendance, 0);
        Assert.assertEquals(34.56f, studentCollection.get(3448734).avgSumAttendance, 0);
        Assert.assertEquals(45.8f, studentCollection.get(3456566).avgSumAttendance, 0);
        Assert.assertEquals(76.8f, studentCollection.get(439579759).avgSumAttendance, 0);
        Assert.assertEquals(99.8f, studentCollection.get(978764565).avgSumAttendance, 0);
    }

    @Test
    public void testInitialPartitionGrades() throws IOException {
        StudentCollection studentCollection = new StudentCollection();
        Student st1 = studentCollection.get(3448734);
        st1.grades = new ArrayList<Integer>();
        st1.grades.add(0, 34);
        Student st2 = studentCollection.get(439579759);
        st2.grades = new ArrayList<Integer>();
        st2.grades.add(0, 76);
        Student st3 = studentCollection.get(3456566);
        st3.grades = new ArrayList<Integer>();
        st3.grades.add(0, 45);
        Student st4 = studentCollection.get(978768763);
        st4.grades = new ArrayList<Integer>();
        st4.grades.add(0, 25);
        Student st5 = studentCollection.get(978764565);
        st5.grades = new ArrayList<Integer>();
        st5.grades.add(0, 99);

        String st = "grades";

        TreeMap<Integer, ArrayList<Student>> result = OneDimensionClusterisation.initialPartition(studentCollection, st, 3, 3, 3);

        Assert.assertEquals("size of cluster map" ,2, result.size());
        Assert.assertEquals("size of cluster 0", 3, result.get(0).size());
        Assert.assertEquals("size of cluster 1", 2, result.get(1).size());
        Assert.assertEquals(true, result.get(0).contains(studentCollection.get(978768763)));
        Assert.assertEquals(true, result.get(0).contains(studentCollection.get(3448734)));
        Assert.assertEquals(true, result.get(0).contains(studentCollection.get(3456566)));
        Assert.assertEquals(true, result.get(1).contains(studentCollection.get(439579759)));
        Assert.assertEquals(true, result.get(1).contains(studentCollection.get(978764565)));

        Assert.assertEquals(25, studentCollection.get(978768763).avgSumGrades, 0);
        Assert.assertEquals(34, studentCollection.get(3448734).avgSumGrades, 0);
        Assert.assertEquals(45, studentCollection.get(3456566).avgSumGrades, 0);
        Assert.assertEquals(76, studentCollection.get(439579759).avgSumGrades, 0);
        Assert.assertEquals(99, studentCollection.get(978764565).avgSumGrades, 0);
    }

    @Test
    public void testFindCentroid() {

        StudentCollection studentCollection = new StudentCollection();
        Student st1 = studentCollection.get(3448734);
        st1.attendencePercentage = new ArrayList<Float>();
        st1.attendencePercentage.add(0, 34.56f);
        st1.grades = new ArrayList<Integer>();
        st1.grades.add(0, 34);
        Student st2 = studentCollection.get(439579759);
        st2.attendencePercentage = new ArrayList<Float>();
        st2.attendencePercentage.add(0, 76.8f);
        st2.grades = new ArrayList<Integer>();
        st2.grades.add(0, 76);
        Student st3 = studentCollection.get(3456566);
        st3.attendencePercentage = new ArrayList<Float>();
        st3.attendencePercentage.add(0, 45.8f);
        st3.grades = new ArrayList<Integer>();
        st3.grades.add(0, 45);

        ArrayList<Student> studentList = new ArrayList<Student>();
        studentList.add(0, st1);
        studentList.add(1, st2);
        studentList.add(2, st3);

        float sum = 157.16f;
        float sumGrades = 155f;
        float result = OneDimensionClusterisation.findCentroid(studentList, sum, "attendance");
        Assert.assertEquals(45.8f, result, 0.00000f);
        float result2 = OneDimensionClusterisation.findCentroid(studentList, sumGrades, "grades");
        Assert.assertEquals(45f, result2, 0.00000f);


    }

    @Test
    public void testFindCentroids() {
        StudentCollection studentCollection = new StudentCollection();
        Student st1 = studentCollection.get(3448734);
        st1.attendencePercentage = new ArrayList<Float>();
        st1.attendencePercentage.add(0, 34.56f);
        st1.grades = new ArrayList<Integer>();
        st1.grades.add(0, 34);
        Student st2 = studentCollection.get(439579759);
        st2.attendencePercentage = new ArrayList<Float>();
        st2.attendencePercentage.add(0, 76.8f);
        st2.grades = new ArrayList<Integer>();
        st2.grades.add(0, 76);
        Student st3 = studentCollection.get(3456566);
        st3.attendencePercentage = new ArrayList<Float>();
        st3.attendencePercentage.add(0, 45.8f);
        st3.grades = new ArrayList<Integer>();
        st3.grades.add(0, 45);
        Student st4 = studentCollection.get(978768763);
        st4.attendencePercentage = new ArrayList<Float>();
        st4.attendencePercentage.add(0, 25.8f);
        st4.grades = new ArrayList<Integer>();
        st4.grades.add(0, 25);
        Student st5 = studentCollection.get(978764565);
        st5.attendencePercentage = new ArrayList<Float>();
        st5.attendencePercentage.add(0, 99.8f);
        st5.grades = new ArrayList<Integer>();
        st5.grades.add(0, 99);

        TreeMap<Integer, ArrayList<Student>> clusterMap = new TreeMap<Integer, ArrayList<Student>>();
        clusterMap.put(0, new ArrayList<Student>());
        clusterMap.put(1, new ArrayList<Student>());
        clusterMap.get(0).add(0, st1);
        clusterMap.get(0).add(1, st2);
        clusterMap.get(0).add(2, st3);
        clusterMap.get(1).add(0, st4);
        clusterMap.get(1).add(1, st5);

        Float[] result = OneDimensionClusterisation.findCentroids(clusterMap, "attendance");

        Assert.assertEquals(2, result.length);
        Assert.assertEquals(45.8f, result[0], 0.000f);
        Assert.assertEquals(99.8f, result[1], 0.000f);

        Float[] result2 = OneDimensionClusterisation.findCentroids(clusterMap, "grades");

        Assert.assertEquals(2, result2.length);
        Assert.assertEquals(45f, result2[0], 0.000f);
        Assert.assertEquals(25f, result2[1], 0.000f);
    }


}
