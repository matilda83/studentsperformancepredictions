package tests;

import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import gai.StudentCollection;
import gai.Student;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 23.08.13
 * Time: 20:41
 * To change this template use File | Settings | File Templates.
 */
public class StudentTest {

    @Test
    public void testGevAvgSum() {

        StudentCollection studentCollection = new StudentCollection();
        studentCollection.get(878687346).attendencePercentage = new ArrayList<Float>();
        studentCollection.get(878687346).grades = new ArrayList<Integer>();
        studentCollection.get(878687346).pagesInternetAccess = new ArrayList<Integer>();

        studentCollection.get(878687346).attendencePercentage.add(0, 34.56f);
        studentCollection.get(878687346).attendencePercentage.add(1, 35.44f);

        studentCollection.get(878687346).grades.add(0, 50);
        studentCollection.get(878687346).grades.add(0, 70);

        studentCollection.get(878687346).pagesInternetAccess.add(0, 78);
        studentCollection.get(878687346).pagesInternetAccess.add(0, 12);

        Float result = studentCollection.get(878687346).getAvgSum("attendance");
        Assert.assertEquals(35, result, 0);

        Float result1 = studentCollection.get(878687346).getAvgSum("grades");
        Assert.assertEquals(60, result1, 0);

        Float result2 = studentCollection.get(878687346).getAvgSum("ia");
        Assert.assertEquals(45, result2, 0);

    }

    @Test
    public void testFindClusterNumber() {

        StudentCollection studentCollection = new StudentCollection();
        studentCollection.get(878687346).attendencePercentage = new ArrayList<Float>();
        studentCollection.get(878687346).grades = new ArrayList<Integer>();
        studentCollection.get(878687346).pagesInternetAccess = new ArrayList<Integer>();

        studentCollection.get(878687341).attendencePercentage = new ArrayList<Float>();
        studentCollection.get(878687341).grades = new ArrayList<Integer>();
        studentCollection.get(878687341).pagesInternetAccess = new ArrayList<Integer>();

        studentCollection.get(878687342).attendencePercentage = new ArrayList<Float>();
        studentCollection.get(878687342).grades = new ArrayList<Integer>();
        studentCollection.get(878687342).pagesInternetAccess = new ArrayList<Integer>();

        studentCollection.clustersOfAttendance = new TreeMap<>();
        studentCollection.clustersOfGrades = new TreeMap<>();
        studentCollection.clustersOfIA = new TreeMap<>();

        studentCollection.clustersOfAttendance.put(0, new ArrayList<Student>());
        studentCollection.get(878687346).attendencePercentage.add(0, 1f);
        studentCollection.clustersOfAttendance.get(0).add(studentCollection.get(878687346));

        studentCollection.clustersOfAttendance.put(1, new ArrayList<Student>());
        studentCollection.get(878687341).attendencePercentage.add(0, 10f);
        studentCollection.clustersOfAttendance.get(1).add(studentCollection.get(878687341));

        studentCollection.clustersOfAttendance.put(2, new ArrayList<Student>());
        studentCollection.get(878687342).attendencePercentage.add(0, 20f);
        studentCollection.clustersOfAttendance.get(2).add(studentCollection.get(878687342));

        studentCollection.clustersOfGrades.put(0, new ArrayList<Student>());
        studentCollection.get(878687346).grades.add(0, 1);
        studentCollection.clustersOfGrades.get(0).add(studentCollection.get(878687346));

        studentCollection.clustersOfGrades.put(1, new ArrayList<Student>());
        studentCollection.get(878687341).grades.add(0, 10);
        studentCollection.clustersOfGrades.get(1).add(studentCollection.get(878687341));

        studentCollection.clustersOfGrades.put(2, new ArrayList<Student>());
        studentCollection.get(878687342).grades.add(0, 20);
        studentCollection.clustersOfGrades.get(2).add(studentCollection.get(878687342));

        studentCollection.clustersOfIA.put(0, new ArrayList<Student>());
        studentCollection.get(878687346).pagesInternetAccess.add(0, 1);
        studentCollection.clustersOfIA.get(0).add(studentCollection.get(878687346));

        studentCollection.clustersOfIA.put(1, new ArrayList<Student>());
        studentCollection.get(878687341).pagesInternetAccess.add(0, 10);
        studentCollection.clustersOfIA.get(1).add(studentCollection.get(878687341));

        studentCollection.clustersOfIA.put(2, new ArrayList<Student>());
        studentCollection.get(878687342).pagesInternetAccess.add(0, 20);
        studentCollection.clustersOfIA.get(2).add(studentCollection.get(878687342));

        Student stResult = new Student(1234567);
        stResult.attendencePercentage = new ArrayList<>();
        stResult.grades = new ArrayList<>();
        stResult.pagesInternetAccess = new ArrayList<>();

        stResult.attendencePercentage.add(0, 2f);
        stResult.grades.add(0, 2);
        stResult.pagesInternetAccess.add(0, 2);

        stResult.findClusterNumber(studentCollection);

        Assert.assertEquals(0, stResult.attendanceCluster);
        Assert.assertEquals(0, stResult.gradesCluster);
        Assert.assertEquals(0, stResult.iaCluster);

        Student stResult2 = new Student(1234567);
        stResult2.attendencePercentage = new ArrayList<>();
        stResult2.grades = new ArrayList<>();
        stResult2.pagesInternetAccess = new ArrayList<>();

        stResult2.attendencePercentage.add(0, 11f);
        stResult2.grades.add(0, 11);
        stResult2.pagesInternetAccess.add(0, 11);

        stResult2.findClusterNumber(studentCollection);

        Assert.assertEquals(1, stResult2.attendanceCluster);
        Assert.assertEquals(1, stResult2.gradesCluster);
        Assert.assertEquals(1, stResult2.iaCluster);

        Student stResult3 = new Student(1234567);
        stResult3.attendencePercentage = new ArrayList<>();
        stResult3.grades = new ArrayList<>();
        stResult3.pagesInternetAccess = new ArrayList<>();

        stResult3.attendencePercentage.add(0, 21f);
        stResult3.grades.add(0, 21);
        stResult3.pagesInternetAccess.add(0, 21);

        stResult3.findClusterNumber(studentCollection);

        Assert.assertEquals(2, stResult3.attendanceCluster);
        Assert.assertEquals(2, stResult3.gradesCluster);
        Assert.assertEquals(2, stResult3.iaCluster);

    }

}
