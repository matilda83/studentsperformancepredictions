package gai;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.util.ArrayList;
import java.util.HashMap;

public class BayesNetworkHelper {

    public static final String V_HOME_STUDENT = "home student";
    public static final String V_SEPTEMBER_STUDENT = "september student";
    public static final String V_FULL_TIME_STUDENT = "full-time student";
    public static final String V_ATTENDANCE = "attendance";
    public static final String V_GRADES = "grades";
    public static final String V_INETRNET_ACTIVITY = "ia";

    private static HashMap<String, NetworkNode> vertexes;

    public void buildNetwork(HashMap<String, ArrayList<Integer>> homeStudents, HashMap<String, ArrayList<Integer>> septSt, HashMap<String, ArrayList<Integer>> fullTimeSt, HashMap<String, ArrayList<Integer>> attClusters, HashMap<String, ArrayList<Integer>> gradesClusters, HashMap<String, ArrayList<Integer>> iaClusters) {
        DirectedGraph<NetworkNode, DefaultEdge> stringGraph = createStringGraph(homeStudents, septSt, fullTimeSt, attClusters, gradesClusters, iaClusters);
        System.out.println(stringGraph.toString());

        BreadthFirstIterator<NetworkNode, DefaultEdge> iter = new BreadthFirstIterator<NetworkNode, DefaultEdge>(stringGraph);
        for (; iter.hasNext(); ) {
            NetworkNode vertex = iter.next();
            System.out.println(vertex);
        }
    }

    private DirectedGraph<NetworkNode, DefaultEdge> createStringGraph(HashMap<String, ArrayList<Integer>> homeStudents, HashMap<String, ArrayList<Integer>> septSt, HashMap<String, ArrayList<Integer>> fullTimeSt, HashMap<String, ArrayList<Integer>> attClusters, HashMap<String, ArrayList<Integer>> gradesClusters, HashMap<String, ArrayList<Integer>> iaClusters) {

        DirectedGraph<NetworkNode, DefaultEdge> g =
            new DefaultDirectedGraph<NetworkNode, DefaultEdge>(DefaultEdge.class);

        vertexes = new HashMap<String, NetworkNode>();

        vertexes.put(V_HOME_STUDENT, new NetworkNode(V_HOME_STUDENT, homeStudents));
        vertexes.put(V_SEPTEMBER_STUDENT, new NetworkNode(V_SEPTEMBER_STUDENT, septSt));
        vertexes.put(V_FULL_TIME_STUDENT, new NetworkNode(V_FULL_TIME_STUDENT, fullTimeSt));
        vertexes.put(V_ATTENDANCE, new NetworkNode(V_ATTENDANCE, attClusters));
        vertexes.put(V_GRADES, new NetworkNode(V_GRADES, gradesClusters));
        vertexes.put(V_INETRNET_ACTIVITY, new NetworkNode(V_INETRNET_ACTIVITY, iaClusters));

        for (NetworkNode node : vertexes.values()) {
            g.addVertex(node);
        }
        // add edges to create a circuit
        g.addEdge(vertexes.get(V_HOME_STUDENT), vertexes.get(V_SEPTEMBER_STUDENT));
        g.addEdge(vertexes.get(V_HOME_STUDENT), vertexes.get(V_FULL_TIME_STUDENT));
        g.addEdge(vertexes.get(V_SEPTEMBER_STUDENT), vertexes.get(V_ATTENDANCE));
        g.addEdge(vertexes.get(V_SEPTEMBER_STUDENT), vertexes.get(V_INETRNET_ACTIVITY));
        g.addEdge(vertexes.get(V_FULL_TIME_STUDENT), vertexes.get(V_ATTENDANCE));
        g.addEdge(vertexes.get(V_FULL_TIME_STUDENT), vertexes.get(V_INETRNET_ACTIVITY));
        g.addEdge(vertexes.get(V_ATTENDANCE), vertexes.get(V_FULL_TIME_STUDENT));
        g.addEdge(vertexes.get(V_INETRNET_ACTIVITY), vertexes.get(V_FULL_TIME_STUDENT));

        return g;
    }

    private class JunctionTreeNode{
        private int id;
        ArrayList<NetworkNode> cliques;
        ArrayList<NetworkNode> separators;

        private JunctionTreeNode(int id, ArrayList<NetworkNode> cliques, ArrayList<NetworkNode> separators) {
            this.id = id;
            this.cliques = cliques;
            this.separators = separators;
        }
    }

    private class JunctionTreeEdge extends DefaultEdge{

    }

    private void buildJunctionTree() {
        SimpleGraph<JunctionTreeNode, JunctionTreeEdge> junctionTree = new SimpleGraph<JunctionTreeNode, JunctionTreeEdge>(JunctionTreeEdge.class);

        ArrayList<NetworkNode> cliques = new ArrayList<NetworkNode>();
        cliques.add(vertexes.get(V_ATTENDANCE));
        cliques.add(vertexes.get(V_INETRNET_ACTIVITY));
        cliques.add(vertexes.get(V_GRADES));
        JunctionTreeNode node1 = new JunctionTreeNode(1, cliques, new ArrayList<NetworkNode>());
        junctionTree.addVertex(node1);

        cliques = new ArrayList<NetworkNode>();
        cliques.add(vertexes.get(V_FULL_TIME_STUDENT));
        cliques.add(vertexes.get(V_ATTENDANCE));
        cliques.add(vertexes.get(V_INETRNET_ACTIVITY));
        ArrayList<NetworkNode> separators = new ArrayList<NetworkNode>();
        separators.add(vertexes.get(V_ATTENDANCE));
        separators.add(vertexes.get(V_INETRNET_ACTIVITY));
        JunctionTreeNode node2 = new JunctionTreeNode(2, cliques, separators);
        junctionTree.addVertex(node2);

        cliques = new ArrayList<NetworkNode>();
        cliques.add(vertexes.get(V_HOME_STUDENT));
        cliques.add(vertexes.get(V_FULL_TIME_STUDENT));
        cliques.add(vertexes.get(V_ATTENDANCE));
        separators = new ArrayList<NetworkNode>();
        separators.add(vertexes.get(V_ATTENDANCE));
        separators.add(vertexes.get(V_FULL_TIME_STUDENT));
        JunctionTreeNode node3 = new JunctionTreeNode(3, cliques, separators);
        junctionTree.addVertex(node3);

        cliques = new ArrayList<NetworkNode>();
        cliques.add(vertexes.get(V_SEPTEMBER_STUDENT));
        cliques.add(vertexes.get(V_HOME_STUDENT));
        separators = new ArrayList<NetworkNode>();
        separators.add(vertexes.get(V_HOME_STUDENT));
        JunctionTreeNode node4 = new JunctionTreeNode(4, cliques, separators);
        junctionTree.addVertex(node4);


        JunctionTreeEdge edge1 = new JunctionTreeEdge();
        junctionTree.addEdge(node3, node4, edge1);


    }

}
