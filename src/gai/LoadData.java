package gai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 20.08.13
 * Time: 11:02
 * To change this template use File | Settings | File Templates.
 */
public class LoadData {

    public static final String DATA = "data\\";
    public static final String DATA_OUT = DATA + "out\\";
    static int dateDiffInd = 0;
    static java.util.Date dateAfter;
    static java.util.Date dateXWinter;
    static java.util.Date dateXSummer;
    static SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


    public static void loadData(StudentCollection students, StudentCollection testStudents, int countOfPages, int dateDiff) throws IOException, ParseException {
        dateAfter = new SimpleDateFormat("dd/MM/yy").parse("01/07/12");
        dateXWinter = new SimpleDateFormat("dd/MM/yy").parse("01/01/12");
        dateXSummer = new SimpleDateFormat("dd/MM/yy").parse("01/07/12");
        dateDiffInd = dateDiff;

        new File(DATA_OUT).mkdirs();

        loadStudentDetails(students);
        System.out.println("Student details have been loaded successfully " + students.size());
        loadStudentAttendance(students);
        System.out.println("Student attendance have been loaded successfully " + students.size());
        loadStudentGradesForElectronicCW(students);
        System.out.println("Student grades have been loaded successfully " + students.size());
        loadStudentGradesForPaperCW(students);
        System.out.println("Student paper grades have been loaded successfully " + students.size());
        ArrayList<String> listOfPopularPages = AnalyseTheMostPopularPages.getTheMostPopularPages(students, countOfPages);
        loadStudentInternetActivity(students, listOfPopularPages);
        students.listOfTheMostPostPages = listOfPopularPages;
        buildTestSet(students, testStudents);
        System.out.println("Student internet activity have been loaded successfully ");

        ArrayList<Student> inCompliteStudents = new ArrayList<>();
        for (Integer id :students.keySet()) {
            Student st = students.get(id);
            if (!st.isCompleteData())
                inCompliteStudents.add(st);
            else if (AnalyseStudentAcademicPerformance.isDataDiffSqr == 2)
                for (int i = 0; i < st.pagesInternetAccess.size(); i++)    {
                    int value = st.pagesInternetAccess.get(i);
                    value = (int) Math.sqrt(value);
                    st.pagesInternetAccess.set(i, value);
                }

        }

        for (Student st: inCompliteStudents)
            students.remove(st.id);

//        dumpStudentListsToFile(students, "list of students");
    }

    private static void buildTestSet(StudentCollection students, StudentCollection testStudentList) {
        int testStudentCount = (students.size() / 100) * 20;
        for (Integer stId : students.keySet()) {

            testStudentList.put(stId, students.get(stId));
            students.get(stId).isTest = true;
            if (testStudentCount == 0) {
                break;
            }
            testStudentCount--;
        }


        for (Integer stId : testStudentList.keySet()) {
            students.remove(stId);
        }
    }


    public static void loadStudentDetails(StudentCollection studentCollection) throws IOException {

        ArrayList<Integer> janStarters = readFileJanuaryStarters(DATA + "1112Janstarters.txt");
        BufferedReader br = ReadFromFile.readFile(DATA + "StudentsDetails1112.txt");

        String line;
        while ((line = br.readLine()) != null) {
            lineAnalysisForStudentDetails(studentCollection, line, janStarters);
        }
        ReadFromFile.CloseFile(br);
    }

    public static void lineAnalysisForStudentDetails(StudentCollection studentCollection, String line, ArrayList<Integer> janStarters) {

        String[] values = line.split("\t");

//        if (values[3].equals("PG")) {      //only postgraduate students are studied
            Integer stId = Integer.valueOf(values[0].trim());
            Student student = studentCollection.get(stId);
            if (values[5].equals("01") || values[5].equals("05")) {
                student.isFullTimeStudent = true;
            }
            else {
                student.isFullTimeStudent = false;
            }

            if (values[2].equals("Home student")) {
                student.isHomeStudent = true;
            }
            else {
                student.isHomeStudent = false;
            }

            if (janStarters.contains(Integer.valueOf(values[0])) == false) {
                student.isSeptemberStarter = true;
            }
            else {
                student.isSeptemberStarter = false;
            }

            if (values[3].equals("PG"))
                student.isPostGrad = true;
            else
                student.isPostGrad = false;

//        }

    }


    static ArrayList<Integer> readFileJanuaryStarters(String fileName) throws IOException {

        ArrayList<Integer> janStartersList = new ArrayList<Integer>();
        String line;
        BufferedReader br = ReadFromFile.readFile(fileName);
        while ((line = br.readLine()) != null) {

            String[] values = line.split("\t");
            if (values[3].equals("PG")) {
                janStartersList.add(Integer.valueOf(values[0]));
            }

        }

        ReadFromFile.CloseFile(br);
        return janStartersList;
    }


    public static void loadStudentAttendance(StudentCollection studentList)
        throws NumberFormatException, IOException {

        String fileName = DATA + "Attendance1112.txt";
        BufferedReader br = ReadFromFile.readFile(fileName);

        String line;
        while ((line = br.readLine()) != null) {
            lineAnalysisForAttendance(studentList, line);
        }
    }

    public static void lineAnalysisForAttendance(StudentCollection studentList, String line) {

        String[] values = line.split(",");
        if (values.length > 0) {

            float perCentOfAttendance = Float.valueOf(values[values.length - 1]);
            int studentID = Integer.valueOf(values[0].trim());

            if (studentList.containsKey(studentID)) {
                Student st = studentList.get(studentID);
                if (st.attendencePercentage == null) {
                    ArrayList<Float> stAttendancePercantage = new ArrayList<Float>();
                    st.attendencePercentage = stAttendancePercantage;
                }
                st.attendencePercentage.add(Float.valueOf(perCentOfAttendance));
            }
        }

    }

    private static void loadStudentGradesForElectronicCW(StudentCollection studentList) throws IOException {

        String fileName = DATA + "CWGrades1112.txt";
        BufferedReader br = ReadFromFile.readFile(fileName);
        String line;
        while ((line = br.readLine()) != null) {
            lineAnalysisForGrades(studentList, line);
        }
        ReadFromFile.CloseFile(br);
    }

    public static void lineAnalysisForGrades(StudentCollection studentList, String line) {

        String[] values = line.split("\t");
        Integer studentID = Integer.valueOf(values[0]);
        if (Float.valueOf(values[3]) > 0 && studentList.containsKey(studentID)) {

            Student st = studentList.get(studentID);
            if (st.grades == null) {
                ArrayList<Integer> stGrades = new ArrayList<Integer>();
                st.grades = stGrades;
            }
            st.grades.add(Integer.valueOf(values[3]));

        }

    }

    private static void loadStudentGradesForPaperCW(StudentCollection studentList) throws IOException {

        String fileName = DATA + "CWPaperGrades1112.txt";
        BufferedReader br = ReadFromFile.readFile(fileName);
        String line;
        while ((line = br.readLine()) != null) {
            lineAnalysisForPaperCWGrades(studentList, line);
        }
        ReadFromFile.CloseFile(br);
    }

    public static void lineAnalysisForPaperCWGrades(StudentCollection studentList, String line) {

        String[] values = line.split("\t");
        Integer studentID = Integer.valueOf(values[0]);
        if (Float.valueOf(values[1]) > 0 && studentList.containsKey(studentID)) {

            Student st = studentList.get(studentID);
            if (st.grades == null) {
                ArrayList<Integer> stGrades = new ArrayList<Integer>();
                st.grades = stGrades;
            }
            st.grades.add(Integer.valueOf(values[1]));

        }


    }


    private static float dateDifference(Date dt) {

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date2.clear();
        date2.set(dt.getYear(), dt.getMonth(), dt.getDay());

        date1.clear();
        if (dt.after(dateXWinter)) {
            date1.set(dateXSummer.getYear(), dateXSummer.getMonth(), dateXSummer.getDay());

        }

        else   {
            date1.set(dateXWinter.getYear(), dateXWinter.getMonth(), dateXWinter.getDay());

        }


        long diff = date1.getTimeInMillis() - date2.getTimeInMillis();
        float dateDiff = (float) diff/(24*60*60*1000);

        if (dateDiff > 182) throw new IllegalArgumentException("hit from wrong year! " + dt);

        return dateDiff;

    }


    private static void loadStudentInternetActivity(StudentCollection listOfStudents, ArrayList<String> listOFPopularPages) throws IOException, ParseException {

        String fileName = DATA + "Logs1113.txt";
        BufferedReader br = ReadFromFile.readFile(fileName);
        String line;

        while ((line = br.readLine()) != null) {

            lineAnalysisForIA(listOfStudents, line, listOFPopularPages);
        }

        ReadFromFile.CloseFile(br);

    }

    static java.util.Date myStringToDate(String s) throws ParseException {

        try {
            return simpleDate.parse(s);
        } catch (ParseException pe) {
            return null;
        }
    }

    public static void lineAnalysisForIA(StudentCollection listOfStudents, String line, ArrayList<String> listOFPopularPages) throws ParseException {

        String[] values = line.split(",");
        if (values.length > 3 ) {

            try {

                Integer studentID = Integer.valueOf(values[2].trim());
                String pageURL = values[1];
                java.util.Date dt = myStringToDate(values[0]);

                if (listOfStudents.containsKey(studentID) && listOFPopularPages.contains(pageURL) && !dt.after(dateAfter)) {
                    Student st = listOfStudents.get(studentID);
                    float dateDiff = 1;
                    if (dateDiffInd == 1)
                        dateDiff = dateDifference(dt);
                    else if (dateDiffInd == 2) {
                        dateDiff = dateDifference(dt);
                        dateDiff = dateDiff*dateDiff;
                    }

                    initialiseArrayForIA(st);
                    getAccessPerPage(listOFPopularPages, st.pagesInternetAccess, pageURL, (int) dateDiff);
                }

            }
            catch (NumberFormatException ex) {
            }
        }
    }

    private static void initialiseArrayForIA(Student st) {

        if (st.pagesInternetAccess == null) {

            ArrayList<Integer> countOfAccess = new ArrayList<Integer>();
            st.pagesInternetAccess = countOfAccess;
            for (int i = 0; i < AnalyseStudentAcademicPerformance.countOfPopulalPages; i++) {
                st.pagesInternetAccess.add(0);
            }

        }

    }

    private static void getAccessPerPage(ArrayList<String> listOFPopularPages, ArrayList<Integer> hitCount, String pageURL, int dateDiffOfHit) {

        Integer pos = listOFPopularPages.indexOf(pageURL);
        if (hitCount.size() < pos+1)
            hitCount.add(dateDiffOfHit);
        else
            hitCount.set(pos, hitCount.get(pos) + dateDiffOfHit);

    }

    public static void dumpStudentListsToFile(StudentCollection listOfStudents, String fileName) throws IOException {
        FileWriter fileWriter = new FileWriter(DATA_OUT + fileName + ".txt");

        StringBuilder sb = new StringBuilder();
        sb.append("Student id");
        sb.append("\t");
        sb.append("home");
        sb.append("\t");
        sb.append("full-time");
        sb.append("\t");
        sb.append("september");
        sb.append("\t");
        sb.append("attendance");
        sb.append("\t");
        sb.append("ia");
        sb.append("\t");
        sb.append("grades");

        fileWriter.write(sb.toString());
        fileWriter.write("\n");
        System.out.println(listOfStudents.size());
        for (Integer i : listOfStudents.keySet()) {

            Student st = listOfStudents.get(i);
            StringBuilder sb1 = new StringBuilder();
            sb1.append(i);
            sb1.append("\t");
            sb1.append(st.isHomeStudent);
            sb1.append("\t");
            sb1.append(st.isFullTimeStudent);
            sb1.append("\t");
            sb1.append(st.isSeptemberStarter);
            sb1.append("\t");

            ArrayList<Float> tempAtt = st.attendencePercentage;
            if (tempAtt != null) {
                for (Float att : tempAtt) {

                    sb1.append(att);
                    sb1.append(", ");
                }
            }

            else

            {
                for (int i1 = 0; i1 < AnalyseStudentAcademicPerformance.clusterCountA; i1++) {
                    sb1.append(0);
                    sb1.append(", ");
                }
            }

            sb1.append("\t");
            ArrayList<Integer> tempIA = st.pagesInternetAccess;
            if (tempIA != null) {
                for (Integer ia : tempIA) {

                    sb1.append(ia);
                    sb1.append(", ");
                }
            }

            else

            {
                for (int i2 = 0; i2 < AnalyseStudentAcademicPerformance.clusterCountI; i2++) {
                    sb1.append(0);
                    sb1.append(", ");
                }
            }

            sb1.append("\t");
            ArrayList<Integer> tempGrades = st.grades;
            if (tempGrades != null) {
                for (Integer g : tempGrades) {

                    sb1.append(g);
                    sb1.append(", ");
                }
            }

            else

            {
                for (int i3 = 0; i3 < AnalyseStudentAcademicPerformance.clusterCountG; i3++) {
                    sb1.append(0);
                    sb1.append(", ");
                }
            }

            fileWriter.write(sb1.toString());
            fileWriter.write("\n");
        }

        fileWriter.close();
        System.out.println("file for checking student data analysed successfully!");
    }
}
