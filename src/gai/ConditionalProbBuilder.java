package gai;

import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 07.08.13
 * Time: 11:29
 * To change this template use File | Settings | File Templates.
 */
public class ConditionalProbBuilder {

    public HashMap<AbstractMap.SimpleEntry<Integer, String>, Float> probabilityTable;
    public HashMap<Integer, AbstractMap.SimpleEntry<Integer, String>> clusterOrder;
    public static int countOfStudents;

    public static void buildProbabilityTables(StudentCollection studentList) throws IOException {

        countOfStudents = sizeOfHashMap(studentList.clustersFullTime);

        studentList.isSeptemberStarterProb = studentSimpleProbabilityTable(studentList, "S");
        studentList.isFullTimeProb = studentSimpleProbabilityTable(studentList, "F");
        studentList.isHomeStudentsProb = studentSimpleProbabilityTable(studentList, "H");
        studentList.isPostGrad = studentSimpleProbabilityTable(studentList, "P");

        studentList.homeGivenSProb = conditionalProbTableOneParOneChild("H", "S", studentList);
        studentList.iaGivenFProb = conditionalProbTableOneParOneChild("I", "F", studentList);
        studentList.attendanceGivenHFProb = conditionalProbTwoParOneChild("A", "H", "F", studentList);
        studentList.gradesGivenAttIAProb = conditionalProbTwoParOneChild("G", "I", "A", studentList);

        dumpProbabilityTablesToFileInt("homeGivenSProb", studentList.homeGivenSProb);
        dumpProbabilityTablesToFileInt("iaGivenFProb", studentList.iaGivenFProb);
        dumpProbabilityTablesToFileInt("attendanceGivenHFProb", studentList.attendanceGivenHFProb);
        dumpProbabilityTablesToFileInt("gradesGivenAttIAProb", studentList.gradesGivenAttIAProb);
        dumpAllProbabilitiesToFileInt("allProbabilities", studentList);


    }

    public static int sizeOfHashMap(TreeMap<Integer, ArrayList<Student>> hashMap) {

        int sum = 0;
        for (Integer key : hashMap.keySet()) {

            sum += hashMap.get(key).size();

        }

        return sum;

    }

    public static  ArrayList<Float> studentSimpleProbabilityTable(StudentCollection studentData, String whatProbability) {

        ArrayList<Float> simpleProbability = new ArrayList<>();
        int studentsCount = 0;
        for (Integer stId : studentData.keySet()) {

            Student student = studentData.get(stId);
            if (!student.isCompleteData()) continue;

            if (whatProbability.equals("S")) {
                if (student.isSeptemberStarter) {
                    studentsCount++;
                }
            }
            else if (whatProbability.equals("H")) {
                if (student.isHomeStudent) {
                    studentsCount++;
                }

            }
            else if (whatProbability.equals("F")) {
                if (student.isFullTimeStudent) {
                    studentsCount++;
                }
            }
            else if (whatProbability.equals("P")) {
                if (student.isPostGrad) {
                    studentsCount++;
                }
            }
        }

        simpleProbability.add(((float)studentsCount) / countOfStudents);
        simpleProbability.add(1 - ((float)studentsCount) / countOfStudents);
        return simpleProbability;
    }


    public static TreeMap<String, Float> conditionalProbTwoParOneChild(String ch, String p1, String p2, StudentCollection studentCollection) {

        TreeMap<Integer, ArrayList<Student>> tch = studentCollection.getParameter(ch);
        TreeMap<Integer, ArrayList<Student>> t1 = studentCollection.getParameter(p1);
        TreeMap<Integer, ArrayList<Student>> t2 = studentCollection.getParameter(p2);
        TreeMap<String, Float> probTable = new TreeMap<>();

        for (Integer cl1 : tch.keySet()) {
            ArrayList<Student> t1StudentList = tch.get(cl1);
            for (Integer cl2 : t1.keySet()) {
                ArrayList<Student> t2StudentList = t1.get(cl2);
                ArrayList<Student> t1t2Intersection = new ArrayList<>();
                t1t2Intersection = addValueToIntersection(t1StudentList, t2StudentList);
                for (Integer cl3 : t2.keySet()) {
                    ArrayList<Student> t3StudentList = t2.get(cl3);
                    ArrayList<Student> t1t2t3Intersection = new ArrayList<>();
                    t1t2t3Intersection = addValueToIntersection(t1t2Intersection, t3StudentList);
                    StringBuilder sb = new StringBuilder();
                    sb.append(ch);
                    sb.append(cl1);
                    sb.append("\t");
                    sb.append(p1);
                    sb.append(cl2);
                    sb.append("\t");
                    sb.append(p2);
                    sb.append(cl3);
                    sb.append("\t");
                    float listSize = Float.valueOf(t1t2t3Intersection.size());
                    float countOFSt = Float.valueOf(countOfStudents);
                    float conditionalProbability = listSize / countOFSt;
                    probTable.put(sb.toString(), conditionalProbability);
                }
            }

        }
        return probTable;
    }

    public static TreeMap<String, Float> conditionalProbTableOneParOneChild(String ch, String p1, StudentCollection studentCollection) {
        TreeMap<String, Float> probabilityTable = new TreeMap<>();
        TreeMap<Integer, ArrayList<Student>> tch = studentCollection.getParameter(ch);
        TreeMap<Integer, ArrayList<Student>> t1 = studentCollection.getParameter(p1);

        for (Integer cl1 : tch.keySet()) {
            ArrayList<Student> t1StudentList = tch.get(cl1);
            for (Integer cl2 : t1.keySet()) {
                ArrayList<Student> t2StudentList = t1.get(cl2);
                ArrayList<Student> t1t2Intersection = new ArrayList<>();
                t1t2Intersection = addValueToIntersection(t1StudentList, t2StudentList);
                StringBuilder sb = new StringBuilder();
                sb.append(ch);
                sb.append(cl1);
                sb.append("\t");
                sb.append(p1);
                sb.append(cl2);
                sb.append("\t");
                float listSize = Float.valueOf(t1t2Intersection.size());
                float countOFSt = Float.valueOf(countOfStudents);
                float probabilityValue = listSize / countOFSt;
                probabilityTable.put(sb.toString(), probabilityValue);
            }
        }
        return probabilityTable;
    }

    public static ArrayList<Student> addValueToIntersection(ArrayList<Student> list1, ArrayList<Student> list2) {
        ArrayList<Student> l1l2Intersection = new ArrayList<>();
        if (list1.size() > 0 && list2.size() > 0) {
            for (int i = 0; i < list1.size(); i++) {
                if (list2.contains(list1.get(i))) {
                    l1l2Intersection.add(list1.get(i));
                }
            }
        }

        return l1l2Intersection;
    }

    public static void dumpProbabilityTablesToFileInt(String fileName, TreeMap<String, Float> probabilityTable) throws IOException {
        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        for (String st : probabilityTable.keySet()) {

            StringBuilder sb = new StringBuilder();
            sb.append(st);
            sb.append(" ");
            sb.append(probabilityTable.get(st));
            sb.append("\t");
            fileWriter.write(sb.toString());
            fileWriter.write("\n");
        }
        fileWriter.close();
        System.out.println("Conditional probability for " + fileName + " have been created successfully!");
    }

    public static void dumpAllProbabilitiesToFileInt(String fileName, StudentCollection stCollection) throws IOException {
        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        TreeMap<Integer, ArrayList<Student>> clustersOfAttendance = stCollection.clustersOfAttendance;
        TreeMap<Integer, ArrayList<Student>> clustersOfGrades = stCollection.clustersOfGrades;
        TreeMap<Integer, ArrayList<Student>> clustersOfIA = stCollection.clustersOfIA;
        TreeMap<Integer, ArrayList<Student>> clustersFullTime = stCollection.clustersFullTime;
        TreeMap<Integer, ArrayList<Student>> clustersHome = stCollection.clustersHome;
        TreeMap<Integer, ArrayList<Student>> clustersPostGrad = stCollection.clustersPostGrad;
        TreeMap<Integer, ArrayList<Student>> clustersSeptemberStarter = stCollection.clustersSeptemberStarter;


        int countOfStudents_ = stCollection.size();
        System.out.println("Size of st collection " + countOfStudents_);

        StringBuilder sb = new StringBuilder();
        sb.append("Attendance clusters probability: ");
        sb.append("\t");

        for (Integer i: clustersOfAttendance.keySet()) {

            float i1 = (float) clustersOfAttendance.get(i).size() / (float) countOfStudents_;
            sb.append(" cluster " + i + " :" + i1);
            sb.append(" ");
            sb.append("\t");

        }

        fileWriter.write(sb.toString());
        fileWriter.write("\n");

        //  *****************************
        // clusters of Grades
        //  *****************************

        StringBuilder sb1 = new StringBuilder();
        sb1.append("Grades clusters probability: ");
        sb1.append("\t");

        for (Integer i: clustersOfGrades.keySet()) {

            float i1 = (float) clustersOfGrades.get(i).size() / (float) countOfStudents_;
            sb1.append(" cluster " + i + " :" + i1);
            sb1.append(" ");
            sb1.append("\t");

        }

        fileWriter.write(sb1.toString());
        fileWriter.write("\n");

        //  *****************************
        // clusters of Intranet Activity
        //  *****************************

        StringBuilder sb2 = new StringBuilder();
        sb2.append("IA clusters probability: ");
        sb2.append("\t");

        for (Integer i: clustersOfIA.keySet()) {

            float i1 = (float) clustersOfIA.get(i).size() / (float) countOfStudents_;
            sb2.append(" cluster " + i + " :" + i1);
            sb2.append(" ");
            sb2.append("\t");

        }

        fileWriter.write(sb2.toString());
        fileWriter.write("\n");


        //  *****************************
        // Home and overseas
        //  *****************************

        StringBuilder sb3 = new StringBuilder();
        sb3.append("Home and overseas students probability: ");
        sb3.append("\t");

        for (Integer i: clustersHome.keySet()) {

            float i1 = (float) clustersHome.get(i).size() / (float) countOfStudents_;
            sb3.append(" cluster " + i + " :" + i1);
            sb3.append(" ");
            sb3.append("\t");

        }

        fileWriter.write(sb3.toString());
        fileWriter.write("\n");


        //  *****************************
        // Full and part time
        //  *****************************

        StringBuilder sb4 = new StringBuilder();
        sb4.append("Full-time and part time students probability: ");
        sb4.append("\t");

        for (Integer i: clustersFullTime.keySet()) {

            float i1 = (float) clustersFullTime.get(i).size() / (float) countOfStudents_;
            sb4.append(" cluster " + i + " :" + i1);
            sb4.append(" ");
            sb4.append("\t");

        }

        fileWriter.write(sb4.toString());
        fileWriter.write("\n");

        //  *****************************
        // PG and UG time
        //  *****************************

        StringBuilder sb5 = new StringBuilder();
        sb5.append("PG and UG students probability: ");
        sb5.append("\t");

        for (Integer i: clustersPostGrad.keySet()) {

            float i1 = (float) clustersPostGrad.get(i).size() / (float) countOfStudents_;
            sb5.append(" cluster " + i + " :" + i1);
            sb5.append(" ");
            sb5.append("\t");

        }

        fileWriter.write(sb5.toString());
        fileWriter.write("\n");

        //  *****************************
        // September startes and UG time
        //  *****************************

        StringBuilder sb6 = new StringBuilder();
        sb6.append("September and January starters: ");
        sb6.append("\t");

        for (Integer i: clustersSeptemberStarter.keySet()) {

            float i1 = (float) clustersSeptemberStarter.get(i).size() / (float) countOfStudents_;
            sb6.append(" cluster " + i + " :" + i1);
            sb6.append(" ");
            sb6.append("\t");

        }

        fileWriter.write(sb6.toString());
        fileWriter.write("\n");

        fileWriter.close();
        System.out.println("Conditional probability for " + fileName + " have been created successfully!");
    }


}