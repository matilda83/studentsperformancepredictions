package gai;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.TreeMap;

public class AnalyseStudentAcademicPerformance {

    public static int clusterCountG = 5;
    public static int clusterCountA = 3;
    public static int clusterCountI = 5;
    public static int countOfPopulalPages = 3;
    public static int distibOfStDev = 100;
    public static int distibOfStDevForEvidence = 80;
    public static int cloudSize = 30;
    public static int isDataDiffSqr = 0;
    public static boolean isPageURLRestriction = false;
    //from 1 month of record to 4 months
    public static int countOfMonthsForPrediction = 1;

    private static String[] files = new String[]{
        "1112Janstarters.txt",
        "Attendance1112.txt",
        "CWGrades1112.txt",
        "CWPaperGrades1112.txt",
        "HeaderSheets1112.txt",
        "Logs1113.txt",
        "StudentsDetails1112.txt",
    };


    public static void main(final String args[]) throws IOException, SQLException, ClassNotFoundException, ParseException {

        boolean filesOk = true;

        for (String fn : files) {
            if (!new File(LoadData.DATA + fn).exists()) {
                filesOk = false;
                System.out.println("Missing file " + fn);
            }
        }

        if (!filesOk) {
            return;
        }

        StudentCollection students = new StudentCollection();
        StudentCollection testStudentList = new StudentCollection();

        buildBBM(students, testStudentList);
//        TreeMap<String, Float> bestBBNIndicators = new TreeMap<>();
        //bestIndicators(bestBBNIndicators, students, testStudentList);
    }

    private static void buildBBM(StudentCollection students, StudentCollection testStudentList) throws IOException, ParseException {
        LoadData.loadData(students, testStudentList, countOfPopulalPages, isDataDiffSqr);
        Clusterisation.clusteriseStudents(students, testStudentList, clusterCountA, clusterCountI, clusterCountG);
        ConditionalProbBuilder.buildProbabilityTables(students);
        Graph2EvidencePropagation.buildProbabilityTables(students, testStudentList);
        Graph1EvidencePropagation.buildProbabilityTables(students, testStudentList);
    }

    private static void bestIndicators(TreeMap<String, Float> bestBBNIndicators, StudentCollection students, StudentCollection testStudentList) throws IOException, ParseException {
        for (int i = 3; i <= countOfPopulalPages; i++) {
            for (int j = 0; j <= isDataDiffSqr; j++) {

                LoadData.loadData(students, testStudentList, i, j);

                for (int att = 1; att <= clusterCountA; att++) {
                    for (int ia = 1; ia <= clusterCountI; ia++) {
                        for (int gr = 1; gr <= clusterCountG; gr++) {
                            Clusterisation.clusteriseStudents(students, testStudentList, att, ia, gr);
                            ConditionalProbBuilder.buildProbabilityTables(students);
                            StringBuilder sb = getStringBuilder(i, j, att, ia, gr);
                            bestBBNIndicators.put(sb.toString() + "gr2", Graph2EvidencePropagation.buildProbabilityTables(students, testStudentList));
                            bestBBNIndicators.put(sb.toString() + "gr1", Graph1EvidencePropagation.buildProbabilityTables(students, testStudentList));

                            System.out.println(i * (j + 1) * att * ia * gr);
                        }
                    }
                }
            }
        }

        Graph2EvidencePropagation.dumpProbabilityTablesToFileInt("the best indicators", bestBBNIndicators);
    }

    private static StringBuilder getStringBuilder(int i, int j, int att, int ia, int gr) {
        StringBuilder sb = new StringBuilder();
        sb.append("PP");
        sb.append(i);
        sb.append("\t");
        sb.append("DD");
        sb.append(j);
        sb.append("\t");
        sb.append("A");
        sb.append(att);
        sb.append("\t");
        sb.append("I");
        sb.append(ia);
        sb.append("\t");
        sb.append("G");
        sb.append(gr);
        sb.append("\t");
        return sb;
    }


}