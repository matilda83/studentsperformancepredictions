package gai;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 03.09.13
 * Time: 12:11
 * To change this template use File | Settings | File Templates.
 */
public class Graph2EvidencePropagation {
    public static int countOfStudents;

    public static float buildProbabilityTables(StudentCollection studentList, StudentCollection testStudentList) throws IOException {

        countOfStudents = sizeOfHashMap(studentList.clustersFullTime);
        HashMap<Student, ArrayList<Float>> evidenceMap = new HashMap<>();

        TreeMap<String, Float> gradesProbability = conditionalProbGradesAndFiveParents(studentList);
        int successPercent = 0;
        for (Integer testStudentID : testStudentList.keySet()) {
            Student testSt = testStudentList.get(testStudentID);
            testSt.findClusterNumber(studentList);


            ArrayList<Float> gradeProbability = findGradeProbability(gradesProbability, testSt);
            evidenceMap.put(testSt, gradeProbability);
            int indMax = maxProbability(evidenceMap.get(testSt));
            if (testSt.gradesCluster == indMax)
                successPercent++;
        }

        StudentList studentList1 = new StudentList(evidenceMap, "Graph 2", testStudentList);
        dumpProbabilityTablesToFileInt("grades and all", gradesProbability);
        dumpEvidenceTablesToFile(evidenceMap, "evidence map graph 2");
        dumpStudentListTablesToFile(studentList, "training set");

        return (float)successPercent/testStudentList.size();
    }

    public static ArrayList<Float> findGradeProbability(TreeMap<String, Float> gradesProb, Student testStudent) {
        ArrayList<Float> listOfValues = new ArrayList<>();

        float gradesGivenA = 0f;
        float gradesGivenI = 0f;
        float gradesGivenH = 0f;
        float gradesGivenF = 0f;
        float gradesGivenS = 0f;
        float gradesGivenP = 0f;
        float gradesSum    = 0f;

        StringBuilder sb = new StringBuilder("A");
        sb.append(String.valueOf(testStudent.attendanceCluster));
        String attCluster = sb.toString();

        StringBuilder sb1 = new StringBuilder("I");
        sb1.append(String.valueOf(testStudent.iaCluster));
        String iaCluster = sb1.toString();

        StringBuilder sb2 = new StringBuilder("H");
        if (testStudent.isHomeStudent) {
            sb2.append("0");
        }
        else {
            sb2.append("1");
        }
        String hCluster = sb2.toString();


        StringBuilder sb3 = new StringBuilder("F");
        if (testStudent.isFullTimeStudent) {
            sb3.append("0");
        }
        else {
            sb3.append("1");
        }
        String fCluster = sb3.toString();

        StringBuilder sb4 = new StringBuilder("S");
        if (testStudent.isSeptemberStarter) {
            sb4.append("0");
        }
        else {
            sb4.append("1");
        }
        String sCluster = sb4.toString();

        StringBuilder sb5 = new StringBuilder("P");
        if (testStudent.isPostGrad) {
            sb5.append("0");
        }
        else {
            sb5.append("1");
        }

        String pCluster = sb5.toString();

        String clCode = "G0";

        for (String st : gradesProb.keySet()) {

            String[] values = st.split("\t");
            String clCodeCurrent = values[0];

            if (clCodeCurrent.equals(clCode)) {
                if (values[1].equals(attCluster)) {
                    gradesGivenA += gradesProb.get(st);
                }

                if (values[2].equals(iaCluster)) {
                    gradesGivenI += gradesProb.get(st);
                }

                if (values[3].equals(hCluster)) {
                    gradesGivenH += gradesProb.get(st);
                }

                if (values[4].equals(fCluster)) {
                    gradesGivenF += gradesProb.get(st);
                }

                if (values[5].equals(sCluster)) {
                    gradesGivenS += gradesProb.get(st);
                }

                if (values[6].equals(pCluster)) {
                    gradesGivenP += gradesProb.get(st);
                }

                gradesSum += gradesProb.get(st);
            }

            else {

                clCode = clCodeCurrent;

                float result = (gradesGivenA * gradesGivenI * gradesGivenH * gradesGivenF * gradesGivenS * gradesGivenP) / (gradesSum * gradesSum * gradesSum * gradesSum * gradesSum);
//                float result = (gradesGivenA * gradesGivenI * gradesGivenH * gradesGivenF * gradesGivenS) / (gradesSum * gradesSum * gradesSum * gradesSum);

                listOfValues.add(result);
                gradesGivenA = 0f;
                gradesGivenI = 0f;
                gradesGivenH = 0f;
                gradesGivenF = 0f;
                gradesGivenS = 0f;
                gradesGivenP = 0f;
                gradesSum = 0f;

                if (values[1].equals(attCluster)) {
                    gradesGivenA += gradesProb.get(st);
                }

                if (values[2].equals(iaCluster)) {
                    gradesGivenI += gradesProb.get(st);
                }

                if (values[3].equals(hCluster)) {
                    gradesGivenH += gradesProb.get(st);
                }

                if (values[4].equals(fCluster)) {
                    gradesGivenF += gradesProb.get(st);
                }

                if (values[5].equals(sCluster)) {
                    gradesGivenS += gradesProb.get(st);
                }

                if (values[6].equals(pCluster)) {
                    gradesGivenP += gradesProb.get(st);
                }

                gradesSum += gradesProb.get(st);


            }

        }

        float result = (gradesGivenA * gradesGivenI * gradesGivenH * gradesGivenF * gradesGivenS * gradesGivenP) / (gradesSum * gradesSum * gradesSum * gradesSum * gradesSum);
//        float result = (gradesGivenA * gradesGivenI * gradesGivenH * gradesGivenF * gradesGivenS) / (gradesSum * gradesSum * gradesSum * gradesSum);


        listOfValues.add(result);

        return listOfValues;
    }

    public static int sizeOfHashMap(TreeMap<Integer, ArrayList<Student>> hashMap) {

        int sum = 0;
        for (Integer key : hashMap.keySet()) {

            sum += hashMap.get(key).size();

        }

        return sum;

    }

    public static TreeMap<String, Float> conditionalProbGradesAndFiveParents(StudentCollection studentCollection) {

        TreeMap<Integer, ArrayList<Student>> tch = studentCollection.getParameter("G");
        TreeMap<Integer, ArrayList<Student>> tA = studentCollection.getParameter("A");
        TreeMap<Integer, ArrayList<Student>> tI = studentCollection.getParameter("I");
        TreeMap<Integer, ArrayList<Student>> tH = studentCollection.getParameter("H");
        TreeMap<Integer, ArrayList<Student>> tF = studentCollection.getParameter("F");
        TreeMap<Integer, ArrayList<Student>> tS = studentCollection.getParameter("S");
        TreeMap<Integer, ArrayList<Student>> tP = studentCollection.getParameter("P");

        TreeMap<String, Float> probTable = new TreeMap<>();

        for (Integer cl1 : tch.keySet()) {
            ArrayList<Student> t1StudentList = tch.get(cl1);
            for (Integer cl2 : tA.keySet()) {
                ArrayList<Student> t2StudentList = tA.get(cl2);
                ArrayList<Student> t1t2Intersection = addValueToIntersection(t1StudentList, t2StudentList);
                for (Integer cl3 : tI.keySet()) {
                    ArrayList<Student> t3StudentList = tI.get(cl3);
                    ArrayList<Student> t1t2t3Intersection = addValueToIntersection(t1t2Intersection, t3StudentList);

                    for (Integer cl4 : tH.keySet()) {
                        ArrayList<Student> t4StudentList = tH.get(cl4);
                        ArrayList<Student> t1t2t3t4Intersection = addValueToIntersection(t1t2t3Intersection, t4StudentList);
                        for (Integer cl5 : tF.keySet()) {
                            ArrayList<Student> t5StudentList = tF.get(cl5);
                            ArrayList<Student> t1t2t3t4t5Intersection = addValueToIntersection(t1t2t3t4Intersection, t5StudentList);

                            for (Integer cl6 : tS.keySet()) {
                                ArrayList<Student> t6StudentList = tS.get(cl6);
                                ArrayList<Student> t1t2t3t4t5t6Intersection = addValueToIntersection(t1t2t3t4t5Intersection, t6StudentList);

                                for (Integer cl7 : tP.keySet()) {
                                    ArrayList<Student> t7StudentList = tP.get(cl7);
                                    ArrayList<Student> t1t2t3t4t5t6t7Intersection = addValueToIntersection(t1t2t3t4t5t6Intersection, t7StudentList);
                                    float listSize = Float.valueOf(t1t2t3t4t5t6t7Intersection.size());
                                    if (listSize > 0) {
                                        float countOFSt = Float.valueOf(countOfStudents);
                                        float conditionalProbability = listSize / countOFSt;

                                        StringBuilder sb = getStringBuilder(cl1, cl2, cl3, cl4, cl5, cl6, cl7);
                                        probTable.put(sb.toString(), conditionalProbability);
                                    }
                                }
                            }
                        }
                    }

                }
            }

        }
        return probTable;
    }

    private static StringBuilder getStringBuilder(Integer cl1, Integer cl2, Integer cl3, Integer cl4, Integer cl5, Integer cl6, Integer cl7) {
        StringBuilder sb = new StringBuilder();
        sb.append("G");
        sb.append(cl1);
        sb.append("\t");
        sb.append("A");
        sb.append(cl2);
        sb.append("\t");
        sb.append("I");
        sb.append(cl3);
        sb.append("\t");
        sb.append("H");
        sb.append(cl4);
        sb.append("\t");
        sb.append("F");
        sb.append(cl5);
        sb.append("\t");
        sb.append("S");
        sb.append(cl6);
        sb.append("\t");
        sb.append("P");
        sb.append(cl7);
        return sb;
    }

    public static ArrayList<Student> addValueToIntersection(ArrayList<Student> list1, ArrayList<Student> list2) {
        ArrayList<Student> l1l2Intersection = new ArrayList<>();
        if (list1.size() > 0 && list2.size() > 0) {
            for (int i = 0; i < list1.size(); i++) {
                if (list2.contains(list1.get(i))) {
                    l1l2Intersection.add(list1.get(i));
                }
            }
        }

        return l1l2Intersection;
    }

    public static int maxProbability(ArrayList<Float> listOfProbabilities) {

        float max = 0f;
        for (float v : listOfProbabilities) {
            if (max < v) {
                max = v;
            }
        }

        int indMax = 0;
        for (int ind = 0; ind < listOfProbabilities.size(); ind++) {
            if (max == listOfProbabilities.get(ind)) {
                indMax = ind;
                break;
            }
        }

        return indMax;

    }

    public static boolean iffClusterInProbabilityCloud(ArrayList<Float> listOfProbabilities, int gradeCluster) {

        int cloudSize = (AnalyseStudentAcademicPerformance.clusterCountG / 100) * AnalyseStudentAcademicPerformance.cloudSize;
        int[] cloudOfProb = new int[cloudSize];
        int indMaxValue = maxProbability(listOfProbabilities);
        float maxValue = listOfProbabilities.get(indMaxValue);
        int listSize = listOfProbabilities.size();
        cloudOfProb[0] = indMaxValue;

        for (Float v : listOfProbabilities) {

            double dev = Math.sqrt(Math.pow(v - maxValue, 2) / listSize);
            if (dev <= AnalyseStudentAcademicPerformance.distibOfStDevForEvidence) {


            }


        }

        return false;
    }

    public static void dumpEvidenceTablesToFile(HashMap<Student, ArrayList<Float>> mapForPrint, String fileName) throws IOException {

        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        for (Student st : mapForPrint.keySet()) {

            ArrayList<Float> tempArrayList = mapForPrint.get(st);
            StringBuilder sb = new StringBuilder();
            float sum = 0f;
            for (Float v : tempArrayList) {
                sum += v;
            }

            sb.append(st.id);
            sb.append("\t");

            ArrayList<Float> tempList = new ArrayList<>();

            for (int i = 0; i < tempArrayList.size(); i++) {
                sb.append(i);
                sb.append("\t");
                float perCentOfProbability = (tempArrayList.get(i) * 100) / sum;
                sb.append(perCentOfProbability);
                sb.append("\t");
                tempList.add(tempArrayList.get(i));
            }

            int indMax = maxProbability(tempList);

            sb.append("avg grade ");
            sb.append(st.getAvgSum("grades"));
            sb.append("\t");
            sb.append(st.gradesCluster);
            sb.append("\t");
            sb.append(indMax);
            sb.append("\t");
            sb.append(st.gradesCluster - indMax);

            sb.append("\t");
            sb.append(st.iaCluster);
            sb.append("\t");
            sb.append(st.attendanceCluster);
            sb.append("\t");
            sb.append(st.isFullTimeStudent);
            sb.append("\t");
            sb.append(st.isHomeStudent);
            sb.append("\t");
            sb.append(st.isSeptemberStarter);
            sb.append("\t");
            sb.append(st.isPostGrad);


            fileWriter.write(sb.toString());
            fileWriter.write("\n");
        }

        fileWriter.close();
        System.out.println("Evidence calc for " + fileName + " have been created successfully!");
    }

    public static void dumpProbabilityTablesToFileInt(String fileName, TreeMap<String, Float> probabilityTable) throws IOException {
        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        for (String st : probabilityTable.keySet()) {

            StringBuilder sb = new StringBuilder();
            sb.append(st);
            sb.append(" ");
            sb.append("\t");
            sb.append(probabilityTable.get(st));
            sb.append("\t");
            fileWriter.write(sb.toString());
            fileWriter.write("\n");
        }
        fileWriter.close();
        System.out.println("Conditional probability for " + fileName + " have been created successfully!");
    }

    public static void dumpStudentListTablesToFile(StudentCollection studentCollection, String fileName) throws IOException {

        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");


        columnsHeading(studentCollection, fileWriter);

        for (Integer id : studentCollection.keySet()) {
            Student st = studentCollection.get(id);
            st.findClusterNumber(studentCollection);
            StringBuilder sb = new StringBuilder();

            sb.append("avg grade ");
            sb.append(st.getAvgSum("grades"));
            sb.append("\t");
            sb.append(st.gradesCluster);
            sb.append("\t");
            sb.append(st.iaCluster);
            sb.append("\t");
            sb.append(st.attendanceCluster);
            sb.append("\t");
            sb.append(st.isFullTimeStudent);
            sb.append("\t");
            sb.append(st.isHomeStudent);
            sb.append("\t");
            sb.append(st.isSeptemberStarter);

            ArrayList<Integer> pageHitsList = st.pagesInternetAccess;
            for (Integer hits : pageHitsList) {
                sb.append("\t");
                sb.append(hits);
            }


            fileWriter.write(st.id + " " + sb.toString());
            fileWriter.write("\n");

        }

        fileWriter.close();
        System.out.println("Evidence for " + fileName + " have been created successfully!");
    }

    private static void columnsHeading(StudentCollection studentCollection, FileWriter fileWriter) throws IOException {
        StringBuilder sb1 = new StringBuilder();
        sb1.append(" id");
        sb1.append("\t");
        sb1.append(" grades");
        sb1.append("\t");
        sb1.append(" ia");
        sb1.append("\t");
        sb1.append(" att");
        sb1.append("\t");
        sb1.append(" f");
        sb1.append("\t");
        sb1.append(" h");
        sb1.append("\t");
        sb1.append(" s");
        sb1.append("\t");
        for (String st : studentCollection.listOfTheMostPostPages) {
            sb1.append(st);
            sb1.append("\t");
        }

        fileWriter.write(sb1.toString());
        fileWriter.write("\n");
    }


}
