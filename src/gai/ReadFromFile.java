package gai;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {

    public static BufferedReader readFile(String fileName) throws IOException {
        BufferedReader br = null;
        try {

            br = new BufferedReader(new FileReader(fileName));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return br;
    }

    public static void CloseFile(BufferedReader br) throws IOException {

        try {
            if (br != null)
                br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
