package gai;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;


public class AnalyseTheMostPopularPages {

    static java.util.Date dateAfter;
    static SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    static int countOfPopulalPages = 0;

    public static ArrayList<String> getTheMostPopularPages(StudentCollection studentList, int countOfPages) throws IOException, ParseException {
        dateAfter = new SimpleDateFormat("dd/MM/yy").parse("01/07/12");
        countOfPopulalPages = countOfPages;

        String fileName = LoadData.DATA + "Logs1113.txt";
        HashMap<String, Integer> pagesMap = getPageMap(fileName, studentList);
        ArrayList<String> listOfPopularPages = getTheListOfPopularPages(pagesMap,
                "ListOf" + String.valueOf(countOfPages) + "PopularPages");

        return listOfPopularPages;
    }

    public static boolean isSuitableURL(String pageURL) {

        StringBuilder sb = new StringBuilder();
        sb.append("/teachmat1112/index0");
        sb.append("\t");
        sb.append("student/courses");
        sb.append("\t");
        sb.append("/teachmat1112/coursework.asp");
        sb.append("\t");
        sb.append("/student/newstoday.asp");
        sb.append("\t");
//        sb.append("/student/mycoursework.asp");
//        sb.append("\t");
//        sb.append("/teachmat1112/upload");
//        sb.append("\t");
//        sb.append("/student/myintranet");
//        sb.append("\t");
//        sb.append("/student/logout");
//        sb.append("\t");
//        sb.append("/teachmat1112/courses");
//        sb.append("\t");
//        sb.append("/student/survey");
//        sb.append("\t");

        String[] values = sb.toString().split("\t");

        for (int i = 0; i < values.length; i++)
        {
            if (pageURL.contains(values[i])) {
                return false;
            }
        }


        return true;
    }


    private static ArrayList<String> getTheListOfPopularPages(HashMap<String, Integer> pagesMap, String fileName)
            throws IOException {

        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");
        ArrayList<String> listOfPopularPages = new ArrayList<String>();

        pagesMap = (HashMap<String, Integer>) sortByValue(pagesMap);

        for (Entry<String, Integer> s : pagesMap.entrySet()) {

            listOfPopularPages.add(s.getKey());
            fileWriter.write(s.getKey() + "\t " + s.getValue());
            fileWriter.write("\n");
            if (listOfPopularPages.size() == countOfPopulalPages)
                break;
        }
        fileWriter.close();
        System.out.println("The most popular pages have been found successfully!");
        return listOfPopularPages;
    }


    private static HashMap<String, Integer> getPageMap(String fileName, StudentCollection studentList) throws IOException, ParseException {

        HashMap<String, Integer> pagesMap = new HashMap<String, Integer>();

        BufferedReader br = ReadFromFile.readFile(fileName);
        String line;

        while ((line = br.readLine()) != null) {
            String[] values = line.split(",");

            java.util.Date dt = myStringToDate(values[0]);
            if (!isSuitableForAnalysis(values, dt))
                continue;
            String pageURL = values[1];


            try {
                Integer studentID = Integer.valueOf(values[2].trim());

                if (!studentList.containsKey(studentID))
                    continue;

                if (pagesMap.get(pageURL) == null)
                    pagesMap.put(pageURL, 1);
                else
                    pagesMap.put(pageURL, pagesMap.get(pageURL) + 1);
            } catch (NumberFormatException ex) {
            }
        }

        ReadFromFile.CloseFile(br);
        return pagesMap;
    }

    static boolean isSuitableForAnalysis(String[] values, Date dt) throws ParseException {

        if (values.length < 3)
            return false;

        if (dt == null)
            return false;

        if (dt.after(dateAfter))
            return false;

        if (AnalyseStudentAcademicPerformance.isPageURLRestriction)
            if (!isSuitableURL(values[1]))
                return false;


        return true;

    }

    static java.util.Date myStringToDate(String s) throws ParseException {

            try {
                return simpleDate.parse(s);
            } catch (ParseException pe) {
                return null;
            }
    }

    public static Map<String, Integer> sortByValue(Map<String, Integer> map) {
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

            public int compare(Map.Entry<String, Integer> m1, Map.Entry<String, Integer> m2) {
                return (m2.getValue()).compareTo(m1.getValue());
            }
        });

        Map<String, Integer> result = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

}
