package gai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 20.08.13
 * Time: 10:20
 * To change this template use File | Settings | File Templates.
 */

public class Student {
    public Student(int id) {
        this.id = id;
        avgSumGrades = new Float(0);
        avgSumAttendance = new Float(0);
        avgSumIA = new Float(0);
    }

    public static Comparator<Student> comparatorAttendance = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("attendance");
            float sum2 = o2.getAvgSum("attendance");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };

    public static Comparator<Student> comparatorGrades = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("grades");
            float sum2 = o2.getAvgSum("grades");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };

    public static Comparator<Student> comparatorIA = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("ia");
            float sum2 = o2.getAvgSum("ia");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };

    public int id;
    public Boolean isSeptemberStarter;
    public Boolean isHomeStudent;
    public Boolean isFullTimeStudent;
    public Boolean isPostGrad;
    public Boolean isTest = false;
    public int attendanceCluster;
    public int gradesCluster;
    public int iaCluster;
    public ArrayList<Float> attendencePercentage;
    public ArrayList<Integer> pagesInternetAccess;
    public ArrayList<Integer> grades;
    public Float avgSumAttendance;
    public Float avgSumIA;
    public Float avgSumGrades;
    public ArrayList<Integer> abnormalPageHitsList;
    public int maxGradeCluster;

    public boolean isCompleteData(){
        return (isSeptemberStarter != null) && (isHomeStudent != null) && (isFullTimeStudent != null) &&
            (attendencePercentage != null) && (pagesInternetAccess != null) && (grades != null)&& (isPostGrad != null);
    }

    public void setClusterNumber(String st, int clNumber) {

         if (st.equals("attendance"))
             attendanceCluster = clNumber;
         else if (st.equals("grades"))
             gradesCluster = clNumber;
         else if (st.equals("ia"))
             iaCluster = clNumber;
    }

    public Float getAvgSum(String st) {

        if (st.equals("attendance")) {

            if (avgSumAttendance == 0) {
                if (attendencePercentage != null && attendencePercentage.size() > 0) {
                    for (int i = 0; i < attendencePercentage.size(); i++) {
                        Float attPercentage = attendencePercentage.get(i);
                        avgSumAttendance += attPercentage;
                    }
                    avgSumAttendance = avgSumAttendance / attendencePercentage.size();
                    return avgSumAttendance;
                }

            }

            return avgSumAttendance;
        }

        if (st.equals("grades")) {
            if (avgSumGrades == 0) {
                if (grades != null && grades.size() > 0) {
                    for (int i = 0; i < grades.size(); i++) {
                        Integer gradeValue = grades.get(i);
                        avgSumGrades += gradeValue;
                    }
                    avgSumGrades = avgSumGrades / grades.size();
                    return avgSumGrades;
                }

            }

            return avgSumGrades;
        }

        if (st.equals("ia")) {


            if (avgSumIA == 0) {
                if (pagesInternetAccess != null && pagesInternetAccess.size() > 0) {
                    for (int i = 0; i < pagesInternetAccess.size(); i++) {
                        Integer iaHits = pagesInternetAccess.get(i);
                        avgSumIA += iaHits;
                    }
                    avgSumIA = avgSumIA / pagesInternetAccess.size();
                    return avgSumIA;
                }
            }

            return avgSumIA;
        }


        return Float.valueOf(0);
    }

    public void findClusterNumber(StudentCollection studentCollection) {

        for (Integer clNumber: studentCollection.clustersOfAttendance.keySet()) {

            if (clNumber.equals(0)) continue;
            ArrayList<Student> clusterOfStudents = studentCollection.clustersOfAttendance.get(clNumber);
            Collections.sort(clusterOfStudents, comparatorAttendance);
            Float attUpBorder = clusterOfStudents.get(0).getAvgSum("attendance");
            if (getAvgSum("attendance") < attUpBorder) {
                attendanceCluster = clNumber - 1;
                break;
            }
            else if (clNumber == studentCollection.clustersOfAttendance.size() - 1)
                attendanceCluster = clNumber;
        }

        for (Integer clNumber: studentCollection.clustersOfGrades.keySet()) {

            if (clNumber.equals(0)) continue;
            ArrayList<Student> clusterOfStudents = studentCollection.clustersOfGrades.get(clNumber);
            Collections.sort(clusterOfStudents, comparatorGrades);
            Float attUpBorder = clusterOfStudents.get(0).getAvgSum("grades");
            if (getAvgSum("grades") < attUpBorder)    {
                gradesCluster = clNumber - 1;
                break;
            }
            else if (clNumber == studentCollection.clustersOfGrades.size() - 1)
                gradesCluster = clNumber;
        }

        for (Integer clNumber: studentCollection.clustersOfIA.keySet()) {

            if (clNumber.equals(0)) continue;
            ArrayList<Student> clusterOfStudents = studentCollection.clustersOfIA.get(clNumber);
            Collections.sort(clusterOfStudents, comparatorIA);
            Float attUpBorder = clusterOfStudents.get(0).getAvgSum("ia");
            if (getAvgSum("ia") < attUpBorder)   {
                iaCluster = clNumber - 1;
                break;
            }
            else  if (clNumber == studentCollection.clustersOfIA.size() -1)
                iaCluster = clNumber;
        }

    }
}

