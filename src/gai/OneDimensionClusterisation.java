package gai;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;


public class OneDimensionClusterisation {

    static int clusterCount = 0;

    public static Comparator<Student> comparatorAttendance = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("attendance");
            float sum2 = o2.getAvgSum("attendance");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };

    public static Comparator<Student> comparatorGrades = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("grades");
            float sum2 = o2.getAvgSum("grades");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };

    public static Comparator<Student> comparatorIA = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("ia");
            float sum2 = o2.getAvgSum("ia");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };

    public static TreeMap<Integer, ArrayList<Student>> initialPartition(StudentCollection allValues, String whatClusterize, int att, int ia, int gr) throws IOException {

//        if (whatClusterize == "ia")
//            RemoveNoseFormIAData.removeNoiseData(allValues);

        ArrayList<Student> studentArrayList = new ArrayList<Student>();
        for (Integer stId : allValues.keySet()) {
            if (!allValues.get(stId).isCompleteData()) {
                continue;
            }
            studentArrayList.add(allValues.get(stId));
        }

        if  (whatClusterize.equals("attendance")) {
            clusterCount = att;
            Collections.sort((List<Student>) studentArrayList, comparatorAttendance);
        }
        else if (whatClusterize.equals("grades")) {
            clusterCount = gr;
            Collections.sort((List<Student>) studentArrayList, comparatorGrades);
        }
        else if (whatClusterize.equals("ia")) {
            clusterCount = ia;
            Collections.sort((List<Student>) studentArrayList, comparatorIA);

        }

        int sizeOfCluster = studentArrayList.size() / clusterCount + 1;
        TreeMap<Integer, ArrayList<Student>> clusterMap = new TreeMap<Integer, ArrayList<Student>>();

        for (int i = 0; i < clusterCount; i++) {
            clusterMap.put(i, new ArrayList<Student>());
        }
        for (int i = 0; i < studentArrayList.size(); i++) {
            clusterMap.get(i / sizeOfCluster).add(studentArrayList.get(i));
        }

        return clusterMap;
    }

    public static int improveClusters(TreeMap<Integer, ArrayList<Student>> clusterMap, String whatClusterize) {

        int movedValuesCount = 0;

        Float[] centroidsArray = findCentroids(clusterMap, whatClusterize);

        for (int i = 0; i < clusterCount; i++) {

            ArrayList<Student> cluster = clusterMap.get(i);
            for (int j = 0; j < cluster.size(); j++) {

                int bestClusterIndex = findBestClusterIndex(cluster.get(j).getAvgSum(whatClusterize), centroidsArray);

                if (bestClusterIndex != i) {
                    cluster.get(j).setClusterNumber(whatClusterize, bestClusterIndex);
                    clusterMap.get(bestClusterIndex).add(cluster.remove(j));
                    movedValuesCount++;
                    j--;
                }

            }

        }

        return movedValuesCount;

    }

    private static int findBestClusterIndex(float value, Float[] centroidsArray) {

        float distance = Math.abs(value - centroidsArray[0]);
        int bestClusterIndex = 0;

        for (int i = 1; i < clusterCount; i++) {

            if (distance > Math.abs(value - centroidsArray[i])) {
                bestClusterIndex = i;
                distance = Math.abs(value - centroidsArray[i]);
            }
        }

        return bestClusterIndex;
    }


    public static Float[] findCentroids(TreeMap<Integer, ArrayList<Student>> clustersMap, String whatClusterise) {

        Float[] centroidsArray = new Float[clusterCount];
        ArrayList<Student> cluster = new ArrayList<Student>();
        float sum = 0;

        for (int i = 0; i < clusterCount; i++) {

            sum = 0;
            cluster = clustersMap.get(i);
            for (int j = 0; j < cluster.size(); j++) {

                sum = sum + cluster.get(j).getAvgSum(whatClusterise);

            }

            centroidsArray[i] = findCentroid(cluster, sum, whatClusterise);
        }
        return centroidsArray;

    }

    public static float findCentroid(ArrayList<Student> cluster, float sum, String whatClusterise) {

        float bestCandidat = cluster.get(0).getAvgSum(whatClusterise);
        float averageValue = sum / cluster.size();

        for (int j = 1; j < cluster.size(); j++) {


            if (Math.abs(bestCandidat - averageValue) > Math.abs(cluster.get(j).getAvgSum(whatClusterise) - averageValue)) {
                bestCandidat = cluster.get(j).getAvgSum(whatClusterise);
            }
        }

        return bestCandidat;
    }

}
