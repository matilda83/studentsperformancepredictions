package gai;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableColumn;

import javax.swing.table.DefaultTableModel;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 03.11.13
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */
public class StudentList  extends JFrame implements ActionListener{

    private JButton btnClose;
    private JTable table1;
    private JLabel lblTitle, lblTestSet, lblTrainSet, lblInlowRisk, lblAverageRisk, lblHighRisk;
    private JTextPane textHighRisk;
    private JTextPane textMediumRisk;
    private JTextPane textLowRisk;

    int highRiskOfFailing = 0;
    int mediumRiskOfFailing = 0;
    int lowRiskOfFailing = 0;

    public StudentList(HashMap<Student, ArrayList<Float>> mapForPrint, String lstTitle, final StudentCollection studentList) {

        setLayout(new FlowLayout());
        setSize(400, 600);
        setTitle(lstTitle);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        table1 = getjTable(mapForPrint);
        countRiskPerentage(studentList);
        columnWidht();
        JScrollPane scrollPane = new JScrollPane(table1);
        table1.setFillsViewportHeight(true);

        JPanel panel;
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(scrollPane);
        add(BorderLayout.CENTER, new JScrollPane(panel));
        add(BorderLayout.NORTH, lblTitle);
        add(BorderLayout.SOUTH, btnClose);

        final JPanel panel2 = new JPanel(new GridLayout(5, 5));
        final JLabel lblStName = new JLabel("Student name: ");
        final JLabel lblStID = new JLabel("Student id: ");
        final JLabel lblIsHome= new JLabel();
        final JLabel lblISFullTime= new JLabel();
        final JLabel lblstSeptStarter = new JLabel();
        final JLabel lblstIsPostgrad = new JLabel();

        final JLabel lblAttCluster = new JLabel();
        final JLabel lblIACluster = new JLabel();
        final JLabel lblRiskyAssessment = new JLabel();


        panel2.add(lblRiskyAssessment);
        panel2.add(lblStName);
        panel2.add(lblStID);
        panel2.add(lblIsHome);
        panel2.add(lblISFullTime);
        panel2.add(lblstSeptStarter);
        panel2.add(lblstIsPostgrad);
        panel2.add(lblAttCluster);
        panel2.add(lblIACluster);
        panel2.add(textHighRisk);
        panel2.add(textMediumRisk);
        panel2.add(textLowRisk);

        add(BorderLayout.WEST, panel2);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);


        table1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                super.mouseClicked(evt);
                int row = table1.rowAtPoint(evt.getPoint());
                int col = table1.columnAtPoint(evt.getPoint());
                if (row >= 0 && col >= 0) {

                    Student st = studentList.get((int) table1.getValueAt(row, col));
                    lblStName.setText("Student name: Name Surname");
                    lblStID.setText("Stuent ID: " + String.valueOf(st.id));
                    String homeSt;
                    if (st.isHomeStudent)
                        homeSt = "Home student";
                    else
                        homeSt = "Overseas student";
                    lblIsHome.setText(homeSt);

                    String fullTime;
                    if (st.isFullTimeStudent)
                        fullTime = "Full-time student";
                    else
                        fullTime = "Part-time student";
                    lblISFullTime.setText(fullTime);

                    String postGrad;
                    if (st.isPostGrad)
                        postGrad = "PG student";
                    else
                        postGrad = "UG student";
                    lblstIsPostgrad.setText(postGrad);

                    String septStarter;
                    if (st.isSeptemberStarter)
                        septStarter = "September starter";
                    else
                        septStarter = "January starter";
                    lblstIsPostgrad.setText(septStarter);


                    lblAttCluster.setText("Attendance cluster: " + String.valueOf(st.attendanceCluster));
                    lblIACluster.setText("IA cluster: " + String.valueOf(st.iaCluster));

                    int maxGradeCluster = (int) table1.getValueAt(row, AnalyseStudentAcademicPerformance.clusterCountG + 1);

                    if (maxGradeCluster == 0 || maxGradeCluster == 1)       {
                        panel2.setBackground(Color.RED);
                        lblRiskyAssessment.setText("High risk of failing");
                    }
                    else if  (maxGradeCluster == 2)                      {
                        panel2.setBackground(Color.YELLOW);
                        lblRiskyAssessment.setText("Normal risk of failing");
                    }
                    else     {
                        panel2.setBackground(Color.GREEN);
                        lblRiskyAssessment.setText("Low risk of failing");
                    }


                }
            }
        });

        btnClose.addActionListener(this);

    }

    private void countRiskPerentage(StudentCollection studentList) {


        for (int i = 0; i < table1.getRowCount(); i++) {
            Student st = studentList.get(table1.getValueAt(i, 0));
            int maxGradeCluster = st.maxGradeCluster;


            if (maxGradeCluster == 0 || maxGradeCluster == 1)       {
               highRiskOfFailing++;
            }
            else if  (maxGradeCluster == 2)                      {
               mediumRiskOfFailing++;
            }
            else     {
                lowRiskOfFailing++;
            }
        }

        textHighRisk.setText("high risk of failing: " + (float) highRiskOfFailing/ (float) studentList.size());
        textHighRisk.setBackground(Color.getColor("PINK"));
        textMediumRisk.setText("normal risk of failing: " + (float) mediumRiskOfFailing/ (float) studentList.size());
        textMediumRisk.setBackground(Color.YELLOW);
        textLowRisk.setText("low risk of failing: " + (float) lowRiskOfFailing/ (float) studentList.size());
        textMediumRisk.setBackground(Color.GREEN);

    }

    private void columnWidht() {
        TableColumn column = null;
        for (int i = 0; i < 5; i++) {
            column = table1.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(100); //third column is bigger
            } else {
                column.setPreferredWidth(50);
            }
        }
    }

    private JTable getjTable(HashMap<Student, ArrayList<Float>> mapForPrint) {

        String[] columnNames = new String[AnalyseStudentAcademicPerformance.clusterCountG+3];
        Object[][] data = new Object[mapForPrint.size()][];
        int k = 0;

        columnNames[0] = "Student ID";
        columnNames[AnalyseStudentAcademicPerformance.clusterCountG+ 1] = "Max cluster";

        for (int i =0; i< AnalyseStudentAcademicPerformance.clusterCountG; i++)
            columnNames[i+1] = "cl" + i;

        DecimalFormat dataFormat = new DecimalFormat("#.##");

        for (Student st : mapForPrint.keySet()) {

            ArrayList<Float> tempArrayList = mapForPrint.get(st);
            float sum = 0f;
            for (Float v : tempArrayList) {
                sum += v;
            }

            data[k] = new Object[AnalyseStudentAcademicPerformance.clusterCountG+2];
            data[k][0] = st.id;


            ArrayList<Float> tempList = new ArrayList<>();

            for (int i = 0; i < tempArrayList.size(); i++) {

                float perCentOfProbability = (tempArrayList.get(i) * 100) / sum;
                data[k][i+1] =  dataFormat.format((double) perCentOfProbability);
                tempList.add(tempArrayList.get(i));
            }



            int indMax = Graph1EvidencePropagation.maxProbability(tempList);
            st.maxGradeCluster = indMax;
            data[k][tempArrayList.size()+ 1] = indMax;
            k++;

    }

        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        return new JTable( model );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.

        if (e.getSource() == btnClose)  {

        }


    }


}
