package gai;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 29.07.13
 * Time: 22:15
 * To change this template use File | Settings | File Templates.
 */
public class NetworkNode {

    public String nodeName;
    public HashMap<String, ArrayList<Integer>> nodeStudentList;

    public NetworkNode(String nodeStringName, HashMap<String, ArrayList<Integer>> studentList) {
        nodeName = nodeStringName;
        nodeStudentList = studentList;
    }

}
