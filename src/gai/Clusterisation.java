package gai;

import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 21.08.13
 * Time: 23:16
 * To change this template use File | Settings | File Templates.
 */
public class Clusterisation {

    public static int clusterCountG = 10;
    public static int clusterCountA = 10;
    public static int clusterCountI = 10;

    public static Comparator<Student> comparatorAttendance = new Comparator<Student>() {

    @Override
    public int compare(Student o1, Student o2) {

        float sum = o1.getAvgSum("attendance");
        float sum2 = o2.getAvgSum("attendance");

        if (sum < sum2) {
            return -1;
        }
        else if (sum > sum2) {
            return 1;
        }
        return 0;
    }
};

    public static Comparator<Student> comparatorGrades = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("grades");
            float sum2 = o2.getAvgSum("grades");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };


    public static Comparator<Student> comparatorIA = new Comparator<Student>() {

        @Override
        public int compare(Student o1, Student o2) {

            float sum = o1.getAvgSum("ia");
            float sum2 = o2.getAvgSum("ia");

            if (sum < sum2) {
                return -1;
            }
            else if (sum > sum2) {
                return 1;
            }
            return 0;
        }
    };


    public static void clusteriseStudents(StudentCollection studentCollection, StudentCollection testStudents, int att, int ia, int gr) throws IOException {

        clusterCountA = att;
        clusterCountI = ia;
        clusterCountG = gr;

        studentCollection.clustersOfAttendance = oneDimensionClusterisation(studentCollection, "attendance");
        studentCollection.clustersOfGrades = oneDimensionClusterisation(studentCollection, "grades");
        studentCollection.clustersOfIA = oneDimensionClusterisation(studentCollection, "ia");
        studentCollection.clustersFullTime = bolleanClusters(studentCollection, "fulltime");
        studentCollection.clustersHome = bolleanClusters(studentCollection, "home");
        studentCollection.clustersSeptemberStarter = bolleanClusters(studentCollection, "september");
        studentCollection.clustersPostGrad = bolleanClusters(studentCollection, "postgrad");


        for (Integer stId : testStudents.keySet()) {
            Student st =  testStudents.get(stId);
            st.findClusterNumber(studentCollection);
            System.out.println(st.grades);
        }

//        studentCollection.clustersPostGrad = bolleanClusters(studentCollection, "postgrad");
        dumpClusterToFile(studentCollection.clustersOfAttendance, "!!!attendance cluster map", "attendance");
        dumpClusterToFile(studentCollection.clustersOfGrades, "!!!grades cluster map", "grades");
        dumpClusterIAToFile(studentCollection, "!!!ia cluster map", "ia");

      }


    public static TreeMap<Integer, ArrayList<Student>> bolleanClusters(StudentCollection studentCollection, String whatClusterise) {

        TreeMap<Integer, ArrayList<Student>> clustersBollean = new TreeMap<Integer, ArrayList<Student>>();
        clustersBollean.put(0, new ArrayList<Student>());
        clustersBollean.put(1, new ArrayList<Student>());

        if (whatClusterise.equals("home")) {

            for (Student st: studentCollection.values()) {

                if (st.isCompleteData()&&st.isHomeStudent)
                    clustersBollean.get(0).add(st);
                else if (st.isCompleteData()&&!st.isHomeStudent)
                    clustersBollean.get(1).add(st);
            }
        }

        else if (whatClusterise.equals("september")) {

            for (Student st: studentCollection.values()) {

                if (st.isCompleteData()&&st.isSeptemberStarter)
                    clustersBollean.get(0).add(st);
                else if (st.isCompleteData()&&!st.isSeptemberStarter)
                    clustersBollean.get(1).add(st);
            }
        }
        else if (whatClusterise.equals("fulltime")) {

            for (Student st: studentCollection.values()) {

                if (st.isCompleteData()&&st.isFullTimeStudent)
                    clustersBollean.get(0).add(st);
                else if (st.isCompleteData()&&!st.isFullTimeStudent)
                    clustersBollean.get(1).add(st);
            }
        }

        else if (whatClusterise.equals("postgrad")) {

            for (Student st: studentCollection.values()) {

                if (st.isCompleteData()&&st.isPostGrad)
                    clustersBollean.get(0).add(st);
                else if (st.isCompleteData()&&!st.isPostGrad)
                    clustersBollean.get(1).add(st);
            }
        }
        return clustersBollean;

    }

    public static TreeMap<Integer, ArrayList<Student>> oneDimensionClusterisation(StudentCollection studentCollection, String whatClusterise) throws IOException {

        TreeMap<Integer, ArrayList<Student>> clusterMapAttendance = OneDimensionClusterisation.initialPartition(studentCollection, whatClusterise, clusterCountA, clusterCountI, clusterCountG);

        for (int i = 0; i < 1000; i++) {
            if (OneDimensionClusterisation.improveClusters(clusterMapAttendance, whatClusterise) == 0)
                break;
        }

        return clusterMapAttendance;
    }

    public static void dumpClusterToFile(TreeMap<Integer, ArrayList<Student>> clusterMap,  String fileName, String whatClusterise) throws IOException {

        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName);

        for (Integer clNum: clusterMap.keySet()) {

            StringBuilder sb = new StringBuilder();
            sb.append("cluster  № ");
            sb.append(clNum);


            ArrayList<Student> arrayList = clusterMap.get(clNum);

            if (whatClusterise.equals("attendance")) {
                Collections.sort(arrayList, comparatorAttendance);
            }

            else if  (whatClusterise.equals("grades"))
                Collections.sort(arrayList, comparatorGrades);

            sb.append("\t");
            sb.append(arrayList.get(0).getAvgSum(whatClusterise));
            sb.append("-");
            sb.append(arrayList.get(arrayList.size() - 1).getAvgSum(whatClusterise));
            sb.append("\t");
            sb.append(clusterMap.get(clNum).size());

            fileWriter.write(sb.toString());
            fileWriter.write("\n");

        }
        fileWriter.close();
    }

    public static void dumpClusterIAToFile(StudentCollection studentCollection,  String fileName, String whatClusterise) throws IOException {

        TreeMap<Integer, ArrayList<Student>> clusterMap = studentCollection.clustersOfIA;
        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName);

        for (Integer clNum: clusterMap.keySet()) {

            StringBuilder sb = new StringBuilder();
            sb.append("cluster  № ");
            sb.append(clNum);

            ArrayList<Student> arrayList = clusterMap.get(clNum);
            Collections.sort(arrayList, comparatorIA);

            sb.append("\t");
            sb.append(arrayList.get(0).getAvgSum(whatClusterise));
            sb.append("-");
            sb.append(arrayList.get(arrayList.size() - 1).getAvgSum(whatClusterise));
            sb.append("\t");
            sb.append(clusterMap.get(clNum).size());

            fileWriter.write(sb.toString());
            fileWriter.write("\n");

        }
        fileWriter.close();

        ArrayList<String> listOfPopularPages = studentCollection.listOfTheMostPostPages;

        FileWriter fileWriter1 = new FileWriter(LoadData.DATA_OUT +fileName+" pagemap");

        StringBuilder sb1 = new StringBuilder();
        sb1.append(" ");
        sb1.append("\t");
        for (String st: listOfPopularPages) {
            sb1.append(st);
            sb1.append("\t");
        }

        fileWriter1.write(sb1.toString());
        fileWriter1.write("\n");

        for (Integer clNum: clusterMap.keySet()) {

            StringBuilder sb = new StringBuilder();
            sb.append("cluster  № ");
            sb.append(clNum);

            ArrayList<Student> studentsInCluster = clusterMap.get(clNum);
            ArrayList<Integer> sumForNumberOfHits = new ArrayList<>();

            for (int i = 0; i < listOfPopularPages.size() - 1; i ++)
                sumForNumberOfHits.add(0);


            for (Student student: studentsInCluster) {

                ArrayList<Integer> pageHits = student.pagesInternetAccess;
                for (int i = 0; i < listOfPopularPages.size() - 1; i ++)
                    sumForNumberOfHits.set(i, sumForNumberOfHits.get(i) + pageHits.get(i));

            }

            for (int i = 0; i < listOfPopularPages.size() - 1; i ++) {
                sb.append("\t");
                if (AnalyseStudentAcademicPerformance.isDataDiffSqr == 0)
                    sb.append(sumForNumberOfHits.get(i));
                else
                    sb.append(sumForNumberOfHits.get(i) / studentsInCluster.size());
                sumForNumberOfHits.set(i, 0);

            }


            fileWriter1.write(sb.toString());
            fileWriter1.write("\n");

        }
        fileWriter1.close();
    }
}
