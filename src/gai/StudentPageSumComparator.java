package gai;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

public class StudentPageSumComparator implements Comparator<Entry<Integer, ArrayList<SimpleEntry<String, Double>>>> {

    @Override
    public int compare(Map.Entry<Integer, ArrayList<SimpleEntry<String, Double>>> o1, Entry<Integer, ArrayList<SimpleEntry<String, Double>>> o2) {
        return sum(o1.getValue()) - sum(o2.getValue());
    }

    private int sum(ArrayList<SimpleEntry<String, Double>> l) {
        int s = 0;
        for (SimpleEntry<String, Double> t : l)
            s += t.getValue();
        return s;
    }
}
