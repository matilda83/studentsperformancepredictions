package gai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 21.08.13
 * Time: 0:10
 * To change this template use File | Settings | File Templates.
 */
public class StudentCollection extends HashMap<Integer, Student> {

    public ArrayList<String> listOfTheMostPostPages;
    public TreeMap<Integer, ArrayList<Student>> clustersOfAttendance;
    public TreeMap<Integer, ArrayList<Student>> clustersOfIA;
    public TreeMap<Integer, ArrayList<Student>> clustersOfGrades;
    public TreeMap<Integer, ArrayList<Student>> clustersHome;
    public TreeMap<Integer, ArrayList<Student>> clustersSeptemberStarter;
    public TreeMap<Integer, ArrayList<Student>> clustersFullTime;
    public TreeMap<Integer, ArrayList<Student>> clustersPostGrad;

    public ArrayList<Float> isHomeStudentsProb = new ArrayList<>();
    public ArrayList<Float> isSeptemberStarterProb = new ArrayList<>();
    public ArrayList<Float> isFullTimeProb = new ArrayList<>();
    public ArrayList<Float> isPostGrad = new ArrayList<>();
    public TreeMap<String, Float> homeGivenSProb;
    public TreeMap<String, Float> attendanceGivenHFProb;
    public TreeMap<String, Float> iaGivenFProb;
    public TreeMap<String, Float> gradesGivenAttIAProb;


    @Override
    public Student get(Object key) {
        if (!containsKey(key)) {
            put((Integer) key, new Student((Integer) key));
        }
        return super.get(key);
    }

    public TreeMap<Integer, ArrayList<Student>> getParameter(String parameter) {
        if (parameter.equals("A")) {
            return clustersOfAttendance;
        }
        else if (parameter.equals("I")) {
            return clustersOfIA;
        }
        else if (parameter.equals("G")) {
            return clustersOfGrades;
        }
        if (parameter.equals("H")) {
            return clustersHome;
        }
        else if (parameter.equals("S")) {
            return clustersSeptemberStarter;
        }
        else if (parameter.equals("F")) {
            return clustersFullTime;
        }
        else if (parameter.equals("P")) {
                return clustersPostGrad;
        }
        return null;
    }

    private HashMap<String, Float> clusterProbabilities = new HashMap<String, Float>();

    public Float getClusterProbability(String cl) {
        if (!clusterProbabilities.containsKey(cl)) {
            switch (cl.substring(0, 1)) {
                case "H":
                    clusterProbabilities.put(cl, isHomeStudentsProb.get(Integer.parseInt(cl.substring(1, 2))));
                    break;
                case "F":
                    clusterProbabilities.put(cl, isFullTimeProb.get(Integer.parseInt(cl.substring(1, 2))));
                    break;
                case "S":
                    clusterProbabilities.put(cl, isSeptemberStarterProb.get(Integer.parseInt(cl.substring(1, 2))));
                    break;
                case "P":
                    clusterProbabilities.put(cl, isPostGrad.get(Integer.parseInt(cl.substring(1, 2))));
                    break;
                default:
                    TreeMap<Integer, ArrayList<Student>> map = getParameter(cl.substring(0, 1));
                    int fullCount = 0;
                    for (ArrayList<Student> cluster : map.values()) {
                        fullCount += cluster.size();
                    }
                    int clusterNum = Integer.parseInt(cl.substring(1, 2));
                    Float result = ((float) map.get(clusterNum).size()) / fullCount;
                    clusterProbabilities.put(cl, result);
                    break;
            }
        }
        return clusterProbabilities.get(cl);
    }


}