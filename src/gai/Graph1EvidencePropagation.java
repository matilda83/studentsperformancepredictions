package gai;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 09.09.13
 * Time: 12:25
 * To change this template use File | Settings | File Templates.
 */
public class Graph1EvidencePropagation {

    public static int countOfStudents;


    public static float buildProbabilityTables(StudentCollection studentList, StudentCollection testStudentList) throws IOException {

        countOfStudents = sizeOfHashMap(studentList.clustersFullTime);
        HashMap<Student, ArrayList<Float>> evidenceMap = new HashMap<>();
        TreeMap<String, Float> clique1GIS = ConditionalProbBuilder.conditionalProbTwoParOneChild("G", "I", "S", studentList);
        TreeMap<String, Float> clique2IA = ConditionalProbBuilder.conditionalProbTableOneParOneChild("I", "A", studentList);
        TreeMap<String, Float> clique3AHF = ConditionalProbBuilder.conditionalProbTwoParOneChild("A", "H", "F", studentList);

        int successPercent = 0;

        for (Integer testStudentID : testStudentList.keySet()) {
            Student testSt = testStudentList.get(testStudentID);
            testSt.findClusterNumber(studentList);
            ArrayList<Float> result = findGradeProbability(studentList, testSt, clique1GIS, clique2IA, clique3AHF);
            evidenceMap.put(testSt, result);
            int indMax = maxProbability(result);
            if (testSt.gradesCluster == indMax)
                successPercent++;

        }

        dumpProbabilityTablesToFileInt("clique1GIS", clique1GIS);
        dumpProbabilityTablesToFileInt("clique2IA", clique2IA);
        dumpProbabilityTablesToFileInt("clique3AHF", clique3AHF);
        dumpEvidenceTablesToFile(evidenceMap, "evidence map graph 1");

        StudentList studentList1 = new StudentList(evidenceMap, "Graph 1", testStudentList);

        return (float) successPercent/testStudentList.size();
    }


    public static int sizeOfHashMap(TreeMap<Integer, ArrayList<Student>> hashMap) {

        int sum = 0;
        for (Integer key : hashMap.keySet()) {

            sum += hashMap.get(key).size();

        }

        return sum;

    }

    public static ArrayList<Float> findGradeProbability(StudentCollection studentList, Student testStudent, TreeMap<String, Float> clique1, TreeMap<String, Float> clique2, TreeMap<String, Float> clique3) {

        ArrayList<Float> gradesProbList = new ArrayList<Float>();

        float separatorI = 0f;
        float cliqueAI = 0f;
        float separatorA = 0f;
        float cliqueAHF = 0f;
        float separatorA_new = 0f;
        float cliqueAI_new = 0f;
        float separatorI_new = 0f;
        float cliqueGIS = 0f;

        StringBuilder sb = new StringBuilder("A");
        sb.append(String.valueOf(testStudent.attendanceCluster));
        String attCluster = sb.toString();

        StringBuilder sb1 = new StringBuilder("I");
        sb1.append(String.valueOf(testStudent.iaCluster));
        String iaCluster = sb1.toString();

        StringBuilder sb2 = new StringBuilder("H");
        if (testStudent.isHomeStudent) {
            sb2.append("0");
        }
        else {
            sb2.append("1");
        }
        String hCluster = sb2.toString();


        StringBuilder sb3 = new StringBuilder("F");
        if (testStudent.isFullTimeStudent) {
            sb3.append("0");
        }
        else {
            sb3.append("1");
        }
        String fCluster = sb3.toString();

        StringBuilder sb4 = new StringBuilder("S");
        if (testStudent.isSeptemberStarter) {
            sb4.append("0");

        }
        else {
            sb4.append("1");
        }
        String sCluster = sb4.toString();
        float probHome = studentList.getClusterProbability(hCluster);
        float probFullTime = studentList.getClusterProbability(fCluster);
        float probSept = studentList.getClusterProbability(sCluster);

        for (String st : clique1.keySet()) {

            String[] values = st.split("\t");

            if (values[1].equals(iaCluster) && values[2].equals(sCluster)) {
                separatorI += clique1.get(st) * probSept;
            }
        }

        for (String st2 : clique2.keySet()) {

            String[] values = st2.split("\t");

            if (values[0].equals(iaCluster) && values[1].equals(attCluster)) {
                cliqueAI = clique2.get(st2) * separatorI;
            }
        }

        separatorA = cliqueAI;

        for (String st3 : clique3.keySet()) {

            String[] values = st3.split("\t");
            if (values[0].equals(attCluster) && values[1].equals(hCluster) && values[2].equals(fCluster)) {
                cliqueAHF = clique3.get(st3) * separatorA * probHome * probFullTime;
            }

        }

        separatorA_new = cliqueAHF;

        cliqueAI_new = cliqueAI * separatorA_new / separatorA;
        separatorI_new = cliqueAI_new;

        for (String st4 : clique1.keySet()) {

            String[] values = st4.split("\t");
            if (values[1].equals(iaCluster) && values[2].equals(sCluster)) {
                cliqueGIS =  clique1.get(st4) * probSept * separatorI_new / separatorI;
                gradesProbList.add(cliqueGIS);
            }

        }

        return gradesProbList;

    }

    public static int maxProbability(ArrayList<Float> listOfProbabilities) {

        float max = 0f;
        for (float v : listOfProbabilities) {
            if (max < v) {
                max = v;
            }
        }

        int indMax = 0;
        for (int ind = 0; ind < listOfProbabilities.size(); ind++) {
            if (max == listOfProbabilities.get(ind)) {
                indMax = ind;
                break;
            }
        }

        return indMax;

    }

    public static void dumpEvidenceTablesToFile(HashMap<Student, ArrayList<Float>> mapForPrint, String fileName) throws IOException {

        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        for (Student st : mapForPrint.keySet()) {

            ArrayList<Float> tempArrayList = mapForPrint.get(st);
            StringBuilder sb = new StringBuilder();
            float sum = 0f;
            for (Float v : tempArrayList) {
                sum += v;
            }

            sb.append(st.id);
            sb.append("\t");

            ArrayList<Float> tempList = new ArrayList<>();

            for (int i = 0; i < tempArrayList.size(); i++) {
                sb.append(i);
                sb.append("\t");
                float perCentOfProbability = (tempArrayList.get(i) * 100) / sum;
                sb.append(perCentOfProbability);
                sb.append("\t");
                tempList.add(tempArrayList.get(i));
            }

            int indMax = maxProbability(tempList);

            sb.append("avg grade ");
            sb.append(st.getAvgSum("grades"));
            sb.append("\t");
            sb.append(st.gradesCluster);
            sb.append("\t");
            sb.append(indMax);
            sb.append("\t");
            sb.append(st.gradesCluster - indMax);

            sb.append("\t");
            sb.append(st.iaCluster);
            sb.append("\t");
            sb.append(st.attendanceCluster);
            sb.append("\t");
            sb.append(st.isFullTimeStudent);
            sb.append("\t");
            sb.append(st.isHomeStudent);
            sb.append("\t");
            sb.append(st.isSeptemberStarter);
            sb.append("\t");
//            sb.append("page hits");
//
//            ArrayList<Integer> pageHitsList = st.pagesInternetAccess;
//            for (Integer hits: pageHitsList) {
//                sb.append("\t");
//                sb.append(hits);
//            }


            fileWriter.write(sb.toString());
            fileWriter.write("\n");

        }

        fileWriter.close();
        System.out.println("Evidence for " + fileName + " have been created successfully!");
    }

    public static void dumpProbabilityTablesToFileInt(String fileName, TreeMap<String, Float> probabilityTable) throws IOException {
        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        for (String st : probabilityTable.keySet()) {

            StringBuilder sb = new StringBuilder();
            sb.append(st);
            sb.append(" ");
            sb.append("\t");
            sb.append(probabilityTable.get(st));
            sb.append("\t");
            fileWriter.write(sb.toString());
            fileWriter.write("\n");
        }
        fileWriter.close();
        System.out.println("Conditional probability for " + fileName + " have been created successfully!");
    }
}
