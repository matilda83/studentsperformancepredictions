package gai;

import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 08.08.13
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
public class BuildProbabilityTables {

    public HashMap<AbstractMap.SimpleEntry<Integer, String>, ArrayList<Integer>> studentListsByClusters;
    public HashMap<Integer, AbstractMap.SimpleEntry<Integer, String>> clusterOrder;

    public static Comparator<AbstractMap.SimpleEntry<Integer, String>> comparator = new Comparator<AbstractMap.SimpleEntry<Integer, String>>() {

        public int compare(AbstractMap.SimpleEntry<Integer, String> o1, AbstractMap.SimpleEntry<Integer, String> o2) {
            if (o1.getKey() < o2.getKey()) {
                return -1;
            }
            else if (o1.getKey() > o2.getKey()) {
                return 1;
            }
            return 0;
        }
    };

    public void fillClusterOrder() {

        clusterOrder = new HashMap<Integer, AbstractMap.SimpleEntry<Integer, String>>();
        for (SimpleEntry<Integer, String> e: studentListsByClusters.keySet())
            clusterOrder.put(e.getKey(), e);
    }

    public void addValuesToListofStudent(String listTitle, Integer stId, int i) {

        boolean recordInHahMap = false;
        for (SimpleEntry<Integer, String> e : studentListsByClusters.keySet()) {
            if (e.getValue().equals(listTitle)) {
                ArrayList<Integer> tempList = studentListsByClusters.get(e);
                tempList.add(stId);
                recordInHahMap = true;
            }
        }


        if (!recordInHahMap) {
            ArrayList<Integer> tempList = new ArrayList<Integer>();
            tempList.add(stId);
            AbstractMap.SimpleEntry<Integer, String> e = new AbstractMap.SimpleEntry<Integer, String>(i, listTitle);
            studentListsByClusters.put(e, tempList);
            clusterOrder.put(i, e);
        }


    }

    public void transformHashMap(HashMap<Integer, ArrayList<AbstractMap.SimpleEntry<Integer, Float>>> hashMap, String nodename) {

        studentListsByClusters = new HashMap<SimpleEntry<Integer, String>, ArrayList<Integer>>();
        clusterOrder = new HashMap<Integer, SimpleEntry<Integer, String>>();

        for (Integer clusterNumber : hashMap.keySet()) {

            ArrayList<AbstractMap.SimpleEntry<Integer, Float>> tempArraylist = hashMap.get(clusterNumber);
            StringBuilder sb = new StringBuilder();
            sb.append(nodename);
            sb.append(clusterNumber);
            sb.append("\t");
            sb.append(tempArraylist.get(0).getValue());
            sb.append("-");
            sb.append(tempArraylist.get(tempArraylist.size() - 1).getValue());
            String listTitle = sb.toString();
            for (AbstractMap.SimpleEntry<Integer, Float> e : tempArraylist) {
                addValuesToListofStudent(listTitle, e.getKey(), clusterNumber);
            }

        }

    }

    public void transformHashMap1(HashMap<Integer, HashMap<Integer, ArrayList<AbstractMap.SimpleEntry<String, Double>>>> hashMap, String nodeName) {

        HashMap<AbstractMap.SimpleEntry<Integer, String>, ArrayList<Integer>> hashMap1 = new HashMap<SimpleEntry<Integer, String>, ArrayList<Integer>>();
        HashMap<Integer, AbstractMap.SimpleEntry<Integer, String>> hashMap2 = new HashMap<Integer, SimpleEntry<Integer, String>>();
        this.studentListsByClusters = hashMap1;
        this.clusterOrder = hashMap2;

        for (Integer clusterNumber : hashMap.keySet()) {

            HashMap<Integer, ArrayList<AbstractMap.SimpleEntry<String, Double>>> tempStudentList = hashMap.get(clusterNumber);
            StringBuilder sb = new StringBuilder();
            sb.append(nodeName);
            sb.append(clusterNumber);
            String listTitle = sb.toString();
            for (Integer stId : tempStudentList.keySet()) {
                addValuesToListofStudent(listTitle, stId, clusterNumber);
            }

        }

    }

    public void dumpProbabilityTablesToFileInt(String fileName) throws IOException {
        FileWriter fileWriter = new FileWriter(LoadData.DATA_OUT + fileName + ".txt");

        for (int i = 0; i < clusterOrder.size(); i++) {

            SimpleEntry<Integer, String> st = clusterOrder.get(i);
            StringBuilder sb = new StringBuilder();
            sb.append(st.getKey());
            sb.append(" ");
            sb.append(st.getValue());
            sb.append("\t");
            sb.append(studentListsByClusters.get(st));
            fileWriter.write(sb.toString());
            fileWriter.write("\n");
        }
        fileWriter.close();
        System.out.println(fileName + " have been printed successfully!");
    }


}
