package gai;

import java.io.IOException;
import java.util.ArrayList;

public class RemoveNoseFormIAData {

    public static ArrayList<Float> averageHitsPerPage;

    static void removeNoiseData(StudentCollection studentCollection) throws IOException {

        int devValue = AnalyseStudentAcademicPerformance.distibOfStDev;
        ArrayList<Integer> maxHitOfPage = new ArrayList<Integer>();
        ArrayList<Student> studentsAbnormalHits = getStudentsWithAbnormalHits(studentCollection, devValue, maxHitOfPage);
        normaliseAbnormalHits(studentsAbnormalHits, maxHitOfPage);

    }

    private static void normaliseAbnormalHits(ArrayList<Student> studentsAbnormalHits, ArrayList<Integer> maxHitOfPage) {
        if (studentsAbnormalHits.size() > 0) {
            for (Student st : studentsAbnormalHits) {
                for (int i = 0; i < st.abnormalPageHitsList.size(); i++) {
                    Integer posOfUbnormalHit = st.abnormalPageHitsList.get(i);
                    int value = maxHitOfPage.get(posOfUbnormalHit);
                    st.pagesInternetAccess.set(posOfUbnormalHit, value);
                }
            }
        }

    }



    private static ArrayList<Student> getStudentsWithAbnormalHits(
        StudentCollection studentCollection, int devValue, ArrayList<Integer> maxHitOfPage) throws IOException {

        ArrayList<Student> studentsAbnormalHits = new ArrayList<Student>();
        averageHitsPerPage = getAverageHitsForPage(studentCollection);

        int mapSize = studentCollection.size();

        standDeviationForPageHits(studentCollection, mapSize);

        for (Integer id : studentCollection.keySet()) {
            Student st = studentCollection.get(id);
            if (st.pagesInternetAccess == null) continue;
            for (int i = 0; i < averageHitsPerPage.size(); i++) {

                if (st.pagesInternetAccess.get(i) > averageHitsPerPage.get(i)*(devValue*2/100)) {
               // double standartDeviation = Math.sqrt(Math.pow(st.pagesInternetAccess.get(i) - averageHitsPerPage.get(i), 2) / mapSize);
                //if (standartDeviation > (double) devValue) {
                    if (st.abnormalPageHitsList == null) {
                        st.abnormalPageHitsList = new ArrayList<Integer>();
                    }
                    st.abnormalPageHitsList.add(i);
                    if (!studentsAbnormalHits.contains(st))
                        studentsAbnormalHits.add(st);
                }
                else {

                    if (maxHitOfPage.size() < i+1) {
                        maxHitOfPage.add(st.pagesInternetAccess.get(i));
                    }
                    else if (maxHitOfPage.get(i) < st.pagesInternetAccess.get(i)) {
                        maxHitOfPage.set(i, st.pagesInternetAccess.get(i));
                    }
                }
            }


        }

                return studentsAbnormalHits;

    }

    private static void standDeviationForPageHits(StudentCollection studentCollection, int mapSize) {
        ArrayList<Double> standDeviation = new ArrayList<>();

        for (Integer id : studentCollection.keySet())   {
            Student st = studentCollection.get(id);
            if (st.pagesInternetAccess == null) continue;
            for (int i = 0; i < averageHitsPerPage.size(); i++) {

                if (standDeviation.size() < i + 1)
                    standDeviation.add(Math.pow(st.pagesInternetAccess.get(i) - averageHitsPerPage.get(i), 2));
                else
                    standDeviation.set(i, Math.pow(st.pagesInternetAccess.get(i) - averageHitsPerPage.get(i), 2) + standDeviation.get(i));
            }

        }


        for (int i = 0; i < standDeviation.size(); i++)
            standDeviation.set(i, Math.sqrt(standDeviation.get(i)/mapSize));
    }

    public static ArrayList<Float> getAverageHitsForPage(StudentCollection studentCollection) {

        ArrayList<Float> averageHitsPerPage = new ArrayList<Float>();

        int mapSize = studentCollection.size();
        for (Integer id : studentCollection.keySet()) {
            Student st = studentCollection.get(id);
            ArrayList<Integer> pageHitsCount = st.pagesInternetAccess;
            if (pageHitsCount == null) continue;
            for (int i =0; i < pageHitsCount.size() - 1; i++) {

                if (averageHitsPerPage.size() < i+1) {
                    averageHitsPerPage.add(i, Float.valueOf(pageHitsCount.get(i)));
                }
                else {
                    averageHitsPerPage.set(i, averageHitsPerPage.get(i) + Float.valueOf(pageHitsCount.get(i)));
                }

            }
        }

        for (int i = 0; i < averageHitsPerPage.size(); i++) {
            averageHitsPerPage.set(i, averageHitsPerPage.get(i) / mapSize);
        }

        return averageHitsPerPage;
    }


}
