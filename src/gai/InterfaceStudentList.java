package gai;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: bivy
 * Date: 03.11.13
 * Time: 18:51
 * To change this template use File | Settings | File Templates.
 */
public class InterfaceStudentList extends JFrame implements ActionListener{

    JTextField frameText = new JTextField(10);
    JButton newButton = new JButton("Press Me");
    JTable studentTable = new JTable(20, 20);


    public InterfaceStudentList() {

        setLayout(new FlowLayout());
        setSize(400, 300);
        setTitle("Title");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        add(frameText);
        add(newButton);
        add(studentTable);
        newButton.addActionListener(this);


        setVisible(true);
        }

    @Override
    public void actionPerformed(ActionEvent e) {
        newButton.setText("Hello World!");
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
